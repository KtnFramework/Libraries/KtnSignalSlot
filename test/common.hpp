/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2021-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#ifndef KTNSIGNALSLOT_TEST_COMMON_HPP
#define KTNSIGNALSLOT_TEST_COMMON_HPP
#include <cstddef>

namespace ktn::SignalSlot::test {
template <template <typename... Args> class C>
struct TestSignalTypes {
  using SignalIntRef = C<int &>;
  using SignalSizeRef = C<size_t &>;
  using SignalVoid = C<>;
};
} // namespace ktn::SignalSlot::test
#endif //  KTNSIGNALSLOT_TEST_COMMON_HPP
