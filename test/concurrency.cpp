/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2021-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <gtest/gtest.h>

#include "common.hpp"

#include <ktnSignalSlot/ktnSignalSlot.hpp>

#include <future>
#include <thread>

using ktn::SignalSlot::ktnSignal;
using ktn::SignalSlot::ktnSignalReceiver;
using ktn::SignalSlot::test::TestSignalTypes;

template <class C>
class concurrency : public ::testing::Test {
public:
  using SignalVoid = typename C::SignalVoid;
};

TYPED_TEST_SUITE(concurrency, ::testing::Types<TestSignalTypes<ktnSignal>>);

TYPED_TEST(concurrency, connections_during_emissions_are_successful) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;

  static std::mutex m;
  static std::mutex m2;
  static std::condition_variable cv;
  static bool ready = false;
  static bool threadStarted = false;
  static bool slotsConnected = false;

  static std::atomic_uint8_t emissionCount = 0;

  // this slot begins after the thread to call it is created,
  // and ends when other slots are connected.
  auto freeSlot = []() {
    ++emissionCount;
    auto lock = std::unique_lock(m);
    cv.wait(lock, [] { return ready; });

    threadStarted = true;
    lock.unlock();
    cv.notify_one();

    auto lock2 = std::unique_lock(m2);
    cv.wait(lock2, [] { return slotsConnected; });
  };

  auto anotherFreeSlot = []() { ++emissionCount; };

  auto lambdaCalled = false;
  auto lambdaSlot = [&lambdaCalled]() {
    ++emissionCount;
    lambdaCalled = true;
  };

  struct Receiver : ktnSignalReceiver {
    void Slot() {
      ++emissionCount;
      SlotCalled = true;
    }
    std::atomic_bool SlotCalled = false;
  } receiver;

  SignalVoid repeater;
  std::atomic_bool repeaterCalled = false;
  std::ignore = repeater.Connect(&receiver, [&] {
    ++emissionCount;
    repeaterCalled = true;
  });

  auto lock = std::unique_lock(m);
  signal.template Connect<void>(freeSlot);
  auto thread = std::thread([&] { signal.Emit(); });
  ready = true;
  auto lock2 = std::unique_lock(m2);
  lock.unlock();
  cv.wait(lock2, [] { return threadStarted; });

  signal.template Connect<void>(anotherFreeSlot);
  std::ignore = signal.Connect(&receiver, lambdaSlot);
  signal.Connect(&receiver, &Receiver::Slot);
  signal.Connect(&repeater);
  signal.Emit();
  slotsConnected = true;

  lock2.unlock();
  cv.notify_one();

  thread.join();

  // check that all connections are successful
  EXPECT_EQ(signal.ConnectionIDs().size(), 5);

  // check that slots connected during emission from a different thread are not called
  EXPECT_EQ(emissionCount, 1);
}

TYPED_TEST(concurrency, duplicate_connections_during_emissions_are_duplicates) {
  ::testing::Test::RecordProperty(
      "Description",
      "This test makes sure that connections that would return duplicate results while idle "
      "also do while a signal is emitting, ");

  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;

  static std::mutex m;
  static std::mutex m2;
  static std::condition_variable cv;
  static bool ready = false;
  static bool threadStarted = false;
  static bool slotsConnected = false;

  static std::atomic_uint8_t emissionCount = 0;

  // this slot begins after the thread to call it is created,
  // and ends when other slots are connected.
  auto freeSlot = []() {
    ++emissionCount;
    auto lock = std::unique_lock(m);
    cv.wait(lock, [] { return ready; });

    threadStarted = true;
    lock.unlock();
    cv.notify_one();

    auto lock2 = std::unique_lock(m2);
    cv.wait(lock2, [] { return slotsConnected; });
  };

  auto anotherFreeSlot = []() { ++emissionCount; };

  struct Receiver : ktnSignalReceiver {
    void Slot() {
      ++emissionCount;
    }
  } receiver;

  SignalVoid repeater;
  std::atomic_bool repeaterCalled = false;
  std::ignore = repeater.Connect(&receiver, [&] {
    ++emissionCount;
    repeaterCalled = true;
  });

  auto lock = std::unique_lock(m);
  signal.template Connect<void>(freeSlot);
  signal.template Connect<void>(anotherFreeSlot);
  signal.Connect(&receiver, &Receiver::Slot);
  signal.Connect(&repeater);
  auto thread = std::thread([&] { signal.Emit(); });
  ready = true;
  auto lock2 = std::unique_lock(m2);
  lock.unlock();
  cv.wait(lock2, [] { return threadStarted; });

  // This test does not apply to lambda slots
  // because each lambda connection is different regardless of the expression.
  EXPECT_EQ(
      signal.template Connect<void>(anotherFreeSlot).Result,
      ktn::SignalSlot::ConnectionResult::Duplicate);
  EXPECT_EQ(
      signal.Connect(&receiver, &Receiver::Slot).Result,
      ktn::SignalSlot::ConnectionResult::Duplicate);
  EXPECT_EQ(signal.Connect(&repeater).Result, ktn::SignalSlot::ConnectionResult::Duplicate);
  signal.Emit();
  slotsConnected = true;

  lock2.unlock();
  cv.notify_one();

  thread.join();

  // check that all connections are successful
  EXPECT_EQ(signal.ConnectionIDs().size(), 4);
  EXPECT_EQ(emissionCount, 4);
}
