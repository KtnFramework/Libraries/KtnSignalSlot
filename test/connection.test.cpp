/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2019-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <gtest/gtest.h>

#include "common.hpp"

#include <ktnSignalSlot/ktnSignalSlot.hpp>

using namespace ktn::SignalSlot;

template <class C>
class connection : public ::testing::Test {
public:
  using SignalVoid = typename C::SignalVoid;
  using SignalIntRef = typename C::SignalIntRef;
};

TYPED_TEST_SUITE(connection, ::testing::Types<test::TestSignalTypes<ktnSignal>>);

TYPED_TEST(connection, class_member_functions) {
  ::testing::Test::RecordProperty(
      "Description",
      "This test demonstrates the ability to connect a signal to a class member function.\n"
      "It must compile for both const and non-const functions.\n"
      "The connected functions are called when the signal is emitted.");

  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  struct Receiver : ktnSignalReceiver {
    void Reset() {
      Slot1Called = false;
      Slot2Called = false;
    }
    void Slot1() {
      Slot1Called = true;
    }
    void Slot2() const {
      Slot2Called = true;
    }
    bool Slot1Called{false};
    mutable bool Slot2Called{false};
  } receiver;

  signal.Connect(&receiver, &Receiver::Slot1);
  EXPECT_EQ(receiver.Slot1Called, false);
  signal.Emit();
  EXPECT_EQ(receiver.Slot1Called, true);
  receiver.Reset();

  signal.Connect(&receiver, &Receiver::Slot2);
  EXPECT_EQ(receiver.Slot1Called, false);
  EXPECT_EQ(receiver.Slot2Called, false);
  signal.Emit();
  EXPECT_EQ(receiver.Slot1Called, true);
  EXPECT_EQ(receiver.Slot2Called, true);
}

void freeFunction1() {
  std::cout << __FUNCTION__ << std::endl;
}

TYPED_TEST(connection, connections_to_free_functions_must_be_successful) {
  ::testing::Test::RecordProperty(
      "Description",
      "This test demonstrates the ability to connect a signal to a free function, "
      "as well as stateless lambda expressions.\n"
      "The connected function is called when the signal is emitted.");

  using SignalVoid = typename TestFixture::SignalVoid;
  using SignalIntRef = typename TestFixture::SignalIntRef;
  SignalVoid signalVoid;
  SignalIntRef signalIntRef;

  const int NUMBER_OF_REPETITIONS = 100;

  auto result_ID1 = signalVoid.Connect(freeFunction1);
  EXPECT_EQ(result_ID1.Result, ConnectionResult::Success);
  EXPECT_EQ(result_ID1.ID, 1);

  auto slot1 = []() { std::cout << __FUNCTION__ << std::endl; };
  auto result_ID2 = signalVoid.template Connect<void>(slot1);
  EXPECT_EQ(result_ID2.Result, ConnectionResult::Success);
  EXPECT_EQ(result_ID2.ID, 2);

  int nRecordedCalls = 0;
  auto slot2 = [](int &eNRecordedCalls) { ++eNRecordedCalls; };
  signalIntRef.template Connect<void>(slot2);
  signalIntRef.Emit(nRecordedCalls);
  EXPECT_EQ(nRecordedCalls, 1);

  nRecordedCalls = 0;
  for (int i = 0; i < NUMBER_OF_REPETITIONS; ++i) {
    signalIntRef.Emit(nRecordedCalls);
  }
  EXPECT_EQ(nRecordedCalls, NUMBER_OF_REPETITIONS);
}

TYPED_TEST(connection, connections_to_lambdas_must_be_successful) {
  using SignalVoid = typename TestFixture::SignalVoid;
  using SignalIntRef = typename TestFixture::SignalIntRef;
  SignalVoid signalVoid;
  SignalIntRef signalIntRef;
  class Receiver : public ktnSignalReceiver {
  public:
    bool SlotCalled = false;
  } receiver;

  auto resultAndId1 = signalVoid.Connect(&receiver, [&receiver]() { receiver.SlotCalled = true; });

  EXPECT_EQ(resultAndId1.Result, ConnectionResult::Success);
  EXPECT_EQ(resultAndId1.ID, 1);
  signalVoid.Emit();
  EXPECT_TRUE(receiver.SlotCalled);
  EXPECT_EQ(signalVoid.DisconnectByID(resultAndId1.ID), DisconnectionResult::Success);

  auto resultAndId2 = signalIntRef.Connect(&receiver, [](int &i) { ++i; });

  EXPECT_EQ(resultAndId2.Result, ConnectionResult::Success);
  EXPECT_EQ(resultAndId2.ID, 1);
  int i = 123;
  signalIntRef.Emit(i);
  EXPECT_EQ(i, 124);
  EXPECT_EQ(signalIntRef.DisconnectByID(resultAndId2.ID), DisconnectionResult::Success);
}

TYPED_TEST(connection, connections_to_same_lambda_return_different_slots) {
  ::testing::Test::RecordProperty(
      "Description",
      "Each instance of a lambda is different due to different closures.\n"
      "Because of this, they cannot be considered the same slot, "
      "even though they might be used in the same place in code.");

  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  class Receiver : public ktnSignalReceiver {
  public:
    unsigned int CallCount = 0;
  } receiver;
  auto slot = [&receiver]() -> void { ++receiver.CallCount; };

  auto resultAndId1 = signal.Connect(&receiver, slot);
  auto resultAndId2 = signal.Connect(&receiver, slot);

  EXPECT_EQ(resultAndId1.Result, ConnectionResult::Success);
  EXPECT_EQ(resultAndId2.Result, ConnectionResult::Success);
  EXPECT_GT(resultAndId1.ID, 0);
  EXPECT_GT(resultAndId2.ID, 0);
  EXPECT_NE(resultAndId1.ID, resultAndId2.ID);

  signal.Emit();
  EXPECT_EQ(receiver.CallCount, 2);
}

TYPED_TEST(connection, connections_to_invalid_signals_must_fail) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  std::unique_ptr<SignalVoid> repeater;

  EXPECT_EQ(signal.Connect(repeater.get()).Result, ConnectionResult::Failure);
}

TYPED_TEST(connection, connections_to_signals_must_be_successful) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  SignalVoid repeater;
  static bool slotCalled = false;
  auto slot = [] { slotCalled = true; };

  repeater.template Connect<void>(slot);
  auto resultAndId = signal.Connect(&repeater);

  EXPECT_EQ(resultAndId.Result, ConnectionResult::Success);
  EXPECT_GT(resultAndId.ID, 0);
  signal.Emit();
  EXPECT_TRUE(slotCalled);
}

TYPED_TEST(connection, connections_to_different_repeaters_return_different_slots) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  SignalVoid repeater1;
  SignalVoid repeater2;
  static bool slot1Called = false;
  static bool slot2Called = false;
  auto slot1 = [] { slot1Called = true; };
  auto slot2 = [] { slot2Called = true; };

  repeater1.template Connect<void>(slot1);
  repeater2.template Connect<void>(slot2);

  auto resultAndId1 = signal.Connect(&repeater1);
  auto resultAndId2 = signal.Connect(&repeater2);
  EXPECT_EQ(resultAndId1.Result, ConnectionResult::Success);
  EXPECT_EQ(resultAndId2.Result, ConnectionResult::Success);
  EXPECT_GT(resultAndId1.ID, 0);
  EXPECT_GT(resultAndId2.ID, 0);
  EXPECT_NE(resultAndId1.ID, resultAndId2.ID);

  signal.Emit();
  EXPECT_TRUE(slot1Called && slot2Called);
}

TYPED_TEST(connection, connections_to_same_repeater_are_duplicates) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  SignalVoid repeater;
  static unsigned int slotCalled = 0;
  auto slot = [] { ++slotCalled; };

  repeater.template Connect<void>(slot);

  auto resultAndId1 = signal.Connect(&repeater);
  auto resultAndId2 = signal.Connect(&repeater);
  EXPECT_EQ(resultAndId1.Result, ConnectionResult::Success);
  EXPECT_EQ(resultAndId2.Result, ConnectionResult::Duplicate);
  EXPECT_GT(resultAndId1.ID, 0);
  EXPECT_GT(resultAndId2.ID, 0);
  EXPECT_EQ(resultAndId1.ID, resultAndId2.ID);

  signal.Emit();
  EXPECT_EQ(slotCalled, 1);
}

TYPED_TEST(connection, connection_to_self_must_fail) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;

  EXPECT_EQ(signal.Connect(&signal).Result, ConnectionResult::Failure);
}

TYPED_TEST(connection, null_receiver_no_connection) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  struct Receiver : public ktnSignalReceiver {
    void Slot() {
      SlotCalled = true;
    }
    bool SlotCalled = false;
  };

  std::unique_ptr<Receiver> receiver;

  auto result_ID = signal.Connect(receiver.get(), &Receiver::Slot);

  EXPECT_EQ(result_ID.ID, 0);
  EXPECT_EQ(result_ID.Result, ConnectionResult::Failure);
  EXPECT_EQ(signal.ConnectionIDs().size(), 0);
}

TYPED_TEST(connection, connection_is_removed_when_receiver_is_deleted) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  struct Receiver : public ktnSignalReceiver {
    void Slot1() {
      std::cout << __FUNCTION__ << std::endl;
    }
    void Slot2() {
      std::cout << __FUNCTION__ << std::endl;
    }
  };

  Receiver *receiver = new Receiver;
  signal.Connect(receiver, &Receiver::Slot1);
  signal.Connect(receiver, &Receiver::Slot2);
  EXPECT_EQ(signal.ConnectionIDs().size(), 2);
  delete receiver;
  EXPECT_EQ(signal.ConnectionIDs().size(), 0);
}

TYPED_TEST(connection, no_duplicate_when_receiver_is_deleted_and_recreated_from_same_pointer) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  struct Receiver : public ktnSignalReceiver {
    void Slot() {}
  };

  auto receiver = new Receiver;
  signal.Connect(receiver, &Receiver::Slot);
  EXPECT_EQ(signal.ConnectionIDs().size(), 1);
  delete receiver;
  EXPECT_EQ(signal.ConnectionIDs().size(), 0);

  receiver = new Receiver;
  EXPECT_EQ(signal.Connect(receiver, &Receiver::Slot).Result, ConnectionResult::Success);

  delete receiver;
}

TYPED_TEST(connection, functions_with_similar_signatures) {
  ::testing::Test::RecordProperty(
      "Description",
      "This test demonstrates the ability to connect a signal to similar functions "
      "of the same object."
      "The signal must not see functions with identical signature from the same class as equal.");

  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  struct Receiver : public ktnSignalReceiver {
    void Slot1() {
      std::cout << __FUNCTION__ << std::endl;
    }
    void Slot2() {
      std::cout << __FUNCTION__ << std::endl;
    }
  } receiver;
  auto slot1 = [] { std::cout << __FUNCTION__ << std::endl; };
  auto slot2 = [] { std::cout << __FUNCTION__ << std::endl; };

  signal.Connect(&receiver, &Receiver::Slot1);
  signal.Connect(&receiver, &Receiver::Slot2);
  signal.template Connect<void>(slot1);
  signal.template Connect<void>(slot2);
  EXPECT_EQ(signal.ConnectionIDs().size(), 4);
}

TYPED_TEST(connection, identifiers_are_reused_when_slots_are_deleted) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  struct Receiver : public ktnSignalReceiver {
    void Slot() {}
  };
  EXPECT_EQ(signal.ConnectionIDs().size(), 0);
  auto receiver1 = std::make_unique<Receiver>();
  auto receiver2 = std::make_unique<Receiver>();
  auto receiver3 = std::make_unique<Receiver>();

  auto result3_1 = signal.Connect(receiver1.get(), &Receiver::Slot);
  auto result3_2 = signal.Connect(receiver2.get(), &Receiver::Slot);
  auto result3_3 = signal.Connect(receiver3.get(), &Receiver::Slot);
  auto connections = signal.ConnectionIDs();
  EXPECT_EQ(result3_1.ID, 1);
  EXPECT_EQ(result3_2.ID, 2);
  EXPECT_EQ(result3_3.ID, 3);
  EXPECT_EQ(connections.size(), 3);
  receiver2.reset();
  auto receiver4 = std::make_unique<Receiver>();
  auto result4 = signal.Connect(receiver4.get(), &Receiver::Slot);
  EXPECT_EQ(result4.ID, 2); // the connection to receiver 4 reuses the ID "2"
}

TYPED_TEST(connection, connection_results) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  struct Receiver : public ktnSignalReceiver {
    void Slot() {}
  };

  Receiver *receiver = nullptr;
  auto resultWithID_Failure = signal.Connect(receiver, &Receiver::Slot);
  EXPECT_EQ(resultWithID_Failure.ID, 0);
  EXPECT_EQ(resultWithID_Failure.Result, ktn::SignalSlot::ConnectionResult::Failure);

  receiver = new Receiver();
  auto resultWithID_Success = signal.Connect(receiver, &Receiver::Slot);
  EXPECT_NE(resultWithID_Success.ID, 0);
  EXPECT_EQ(resultWithID_Success.Result, ktn::SignalSlot::ConnectionResult::Success);

  auto resultWithID_Duplicate = signal.Connect(receiver, &Receiver::Slot);
  EXPECT_EQ(resultWithID_Duplicate.ID, resultWithID_Success.ID);
  EXPECT_EQ(resultWithID_Duplicate.Result, ktn::SignalSlot::ConnectionResult::Duplicate);
  delete receiver;
}
