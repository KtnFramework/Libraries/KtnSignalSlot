/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2019-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <gtest/gtest.h>

#include <ktnSignalSlot/ktnSignalSlot.hpp>

#include <chrono>
#include <string>

using namespace ktn::SignalSlot;

template <template <typename... Args> class T>
struct DisconnTestSignals {
  T<> SignalVoid;
  T<> SignalVoidRepeater;
  T<float> SignalFloat;
};

using SignalTypes = ::testing::Types<DisconnTestSignals<ktnSignal>>;

template <class DisconnTestSignals>
class disconnection : public ::testing::Test {
public:
  DisconnTestSignals Signals;
};

TYPED_TEST_SUITE(disconnection, SignalTypes);

// free slots: funcions with matching signals of arbitrary return types
bool freeslot1() {
  return true;
}

void freeslot2_1(float /*eFloat*/) {
  std::cout << __FUNCTION__ << std::endl;
}

void freeslot2_2(float /*eFloat*/) {
  std::cout << __FUNCTION__ << std::endl;
}

class Receiver : public ktnSignalReceiver {
public:
  // these slots are functions with matching signals of arbitrary return types
  float slot1() {
    return 1.2F;
  }

  std::string slot2(float /*eFloat*/) {
    return "OK";
  }

  size_t slot3(long /*eLong*/, std::string /*eString*/) {
    return 11111111111111;
  }
};

TYPED_TEST(disconnection, free_functions) {
  this->Signals.SignalFloat.Connect(freeslot2_1);
  this->Signals.SignalFloat.template Connect<void>(freeslot2_2);
  EXPECT_EQ(this->Signals.SignalFloat.ConnectionIDs().size(), 2);

  auto result1 = this->Signals.SignalFloat.Disconnect(freeslot2_1);
  EXPECT_EQ(result1, DisconnectionResult::Success);
  EXPECT_EQ(this->Signals.SignalFloat.ConnectionIDs().size(), 1);

  auto result2 = this->Signals.SignalFloat.Disconnect(freeslot2_2);
  EXPECT_EQ(result2, DisconnectionResult::Success);
  EXPECT_EQ(this->Signals.SignalFloat.ConnectionIDs().size(), 0);
}

TYPED_TEST(disconnection, class_member_functions) {
  ::testing::Test::RecordProperty(
      "Description",
      "This test demonstrates the ability to disconnect a signal from a class member function.\n"
      "It must compile for both const and non-const functions.\n");

  class Receiver : public ktnSignalReceiver {
  public:
    void slot1() {
      std::cout << __FUNCTION__ << std::endl;
    }
    void slot2() const {
      std::cout << __FUNCTION__ << std::endl;
    }
  };
  auto slot = []() { std::cout << __FUNCTION__ << std::endl; };

  Receiver receiver;
  this->Signals.SignalVoid.template Connect<void>(slot);
  this->Signals.SignalVoid.Connect(&receiver, &Receiver::slot1);
  this->Signals.SignalVoid.Connect(&receiver, &Receiver::slot2);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 3);

  this->Signals.SignalVoid.template Disconnect<void>(slot);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 2);

  this->Signals.SignalVoid.Disconnect(&receiver, &Receiver::slot1);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 1);

  this->Signals.SignalVoid.Disconnect(&receiver, &Receiver::slot2);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 0);
}

TYPED_TEST(disconnection, disconnecting_signals_must_be_successful) {
  this->Signals.SignalVoid.Connect(&this->Signals.SignalVoidRepeater);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 1);
  auto result = this->Signals.SignalVoid.Disconnect(&this->Signals.SignalVoidRepeater);

  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 0);
  EXPECT_EQ(result, DisconnectionResult::Success);
}

TYPED_TEST(disconnection, by_identifiers) {
  auto lambda1 = []() {};

  Receiver receiver;
  auto result1 = this->Signals.SignalVoid.Connect(&freeslot1);
  auto result2 = this->Signals.SignalVoid.template Connect<void>(lambda1);
  auto result3 = this->Signals.SignalVoid.Connect(&receiver, &Receiver::slot1);
  auto result4 = this->Signals.SignalVoid.Connect(&this->Signals.SignalVoidRepeater);

  EXPECT_EQ(result1.ID, 1);
  EXPECT_EQ(result2.ID, 2);
  EXPECT_EQ(result3.ID, 3);
  EXPECT_EQ(result4.ID, 4);

  EXPECT_EQ(result1.Result, ConnectionResult::Success);
  EXPECT_EQ(result2.Result, ConnectionResult::Success);
  EXPECT_EQ(result3.Result, ConnectionResult::Success);
  EXPECT_EQ(result4.Result, ConnectionResult::Success);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 4);

  this->Signals.SignalVoid.DisconnectByID(result1.ID);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 3);
  this->Signals.SignalVoid.DisconnectByID(result2.ID);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 2);
  this->Signals.SignalVoid.DisconnectByID(result3.ID);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 1);
  this->Signals.SignalVoid.DisconnectByID(result4.ID);
  EXPECT_EQ(this->Signals.SignalVoid.ConnectionIDs().size(), 0);

  // disconnection by IDs that do not correspond to a connection is not necessary,
  // and is reported as such
  EXPECT_EQ(this->Signals.SignalVoid.DisconnectByID(10), DisconnectionResult::NotNecessary);
}

TYPED_TEST(disconnection, disconnection_by_id_removes_correct_slot) {
  struct Receiver : public ktnSignalReceiver {
    void Slot() {
      SlotCalled = true;
    }
    bool SlotCalled = false;
  };
  constexpr int NUMBER_OF_RECEIVERS(100);
  std::vector<Receiver> receivers(100);
  for (auto &r : receivers) {
    this->Signals.SignalVoid.Connect(&r, &Receiver::Slot);
  }

  // disconnect random slots
  srand(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));
  std::set<size_t> indices_slotsToBeRemoved;
  for (int i = 0; i < NUMBER_OF_RECEIVERS / 2; ++i) {
    indices_slotsToBeRemoved.insert((rand() % NUMBER_OF_RECEIVERS) + 1);
  }

  for (auto &index : indices_slotsToBeRemoved) {
    this->Signals.SignalVoid.DisconnectByID(index);
  }

  for (size_t i = 0; i < NUMBER_OF_RECEIVERS; ++i) {
    if (indices_slotsToBeRemoved.find(i + 1) != indices_slotsToBeRemoved.end()) {
      EXPECT_EQ(receivers.at(i).Signals().size(), 0);
    } else {
      EXPECT_EQ(receivers.at(i).Signals().size(), 1);
    }
  }
}

TYPED_TEST(disconnection, disconnection_results) {
  auto slot = [] {};
  auto result1 = this->Signals.SignalVoid.template Disconnect<void>(slot);
  EXPECT_EQ(result1, DisconnectionResult::NotNecessary);
  this->Signals.SignalVoid.template Connect<void>(slot);
  auto result2 = this->Signals.SignalVoid.template Disconnect<void>(slot);
  EXPECT_EQ(result2, DisconnectionResult::Success);
  auto result3 = this->Signals.SignalVoid.template Disconnect<void>(slot);
  EXPECT_EQ(result3, DisconnectionResult::NotNecessary);

  Receiver receiver;
  this->Signals.SignalVoid.Connect(&receiver, &Receiver::slot1);
  EXPECT_EQ(
      this->Signals.SignalVoid.Disconnect(&receiver, &Receiver::slot1),
      DisconnectionResult::Success);
  EXPECT_EQ(
      this->Signals.SignalVoid.Disconnect(&receiver, &Receiver::slot1),
      DisconnectionResult::NotNecessary);
}
