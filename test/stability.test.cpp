/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2020-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <gtest/gtest.h>

#include "common.hpp"

#include <ktnSignalSlot/ktnSignalSlot.hpp>

#include <future>
#include <thread>

using namespace ktn::SignalSlot;

template <class C>
class stability : public ::testing::Test {
public:
  using SignalVoid = typename C::SignalVoid;
};

TYPED_TEST_SUITE(stability, ::testing::Types<test::TestSignalTypes<ktnSignal>>);

TYPED_TEST(stability, no_deadlock_or_crash_when_removing_signal_while_calling_slots) {
  using SignalVoid = typename TestFixture::SignalVoid;
  class Receiver : public ktnSignalReceiver {
  public:
    void Slot() {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
  };

  auto receiver = std::make_unique<Receiver>();

  auto signal = std::make_unique<SignalVoid>();
  signal->Connect(receiver.get(), &Receiver::Slot);
  auto t = std::thread(&ktnSignal<>::Emit, signal.get());
  std::this_thread::sleep_for(std::chrono::milliseconds(5));
  signal.reset();
  t.join();
  EXPECT_EQ(receiver->Signals().size(), 0);
}

TYPED_TEST(stability, no_deadlock_or_crash_when_receiver_is_removed_during_emission) {
  using SignalVoid = typename TestFixture::SignalVoid;
  class Receiver : public ktnSignalReceiver {
  public:
    void Slot() {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
  };
  const static unsigned int threadCount = std::thread::hardware_concurrency();

  SignalVoid signal;
  std::vector<std::future<unsigned int>> futures;
  futures.reserve(threadCount);

  for (unsigned int i = 0; i < threadCount; ++i) {
    auto callback = [&signal, i]() -> unsigned int {
      Receiver r;
      signal.Connect(&r, &Receiver::Slot);
      signal.Emit();
      return i;
    };

    futures.emplace_back(std::async(std::launch::async, callback));
  }

  int final = 0;
  for (auto &future : futures) {
    final += future.get();
  }
}
