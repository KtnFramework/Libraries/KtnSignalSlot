/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2019-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <gtest/gtest.h>

#include <ktnSignalSlot/ktnSignalSlot.hpp>

using namespace ktn::SignalSlot;

TEST(lifetime, move_assigned_signal_is_connected_to_receiver) {
  ktnSignal<> signal;
  class Receiver : public ktnSignalReceiver {
  public:
    void operator()() {
      Called = true;
    }
    bool Called = false;
  };

  auto receiver = std::make_unique<Receiver>();
  signal.Connect(receiver.get(), &Receiver::operator());
  EXPECT_EQ(receiver->Signals().size(), 1);

  receiver->Called = false;
  auto moveAssigned = ktnSignal<>();
  moveAssigned = std::move(signal);
  EXPECT_EQ(receiver->Signals().size(), 1);
  signal.Emit();
  EXPECT_FALSE(receiver->Called);
  moveAssigned.Emit();
  EXPECT_TRUE(receiver->Called);
}

TEST(lifetime, move_assigned_signal_is_not_connected_to_previous_receiver) {
  ktnSignal<> signal;
  class Receiver : public ktnSignalReceiver {
  public:
    void operator()() {
      Called = true;
    }
    bool Called = false;
  };

  auto receiver = std::make_unique<Receiver>();
  signal.Connect(receiver.get(), &Receiver::operator());
  EXPECT_EQ(receiver->Signals().size(), 1);

  receiver->Called = false;
  auto moveAssigned = ktnSignal<>();
  auto receiverForMoveAssigned = std::make_unique<Receiver>();
  moveAssigned.Connect(receiverForMoveAssigned.get(), &Receiver::operator());
  moveAssigned.Emit();
  EXPECT_TRUE(receiverForMoveAssigned->Called);
  receiverForMoveAssigned->Called = false;

  moveAssigned = std::move(signal);
  EXPECT_EQ(receiverForMoveAssigned->Signals().size(), 0);
  moveAssigned.Emit();
  EXPECT_FALSE(receiverForMoveAssigned->Called);
}

TEST(lifetime, move_constructed_signal_is_connected_to_receiver) {
  ktnSignal<> signal;
  class Receiver : public ktnSignalReceiver {
  public:
    void operator()() {
      Called = true;
    }
    bool Called = false;
  };

  auto receiver = new Receiver;
  signal.Connect(receiver, &Receiver::operator());
  EXPECT_EQ(receiver->Signals().size(), 1);

  receiver->Called = false;
  auto moveConstructed(std::move(signal));
  EXPECT_EQ(receiver->Signals().size(), 1);
  signal.Emit();
  EXPECT_FALSE(receiver->Called);
  moveConstructed.Emit();
  EXPECT_TRUE(receiver->Called);
  delete receiver;
}

TEST(lifetime, references_to_signals_are_removed_from_receiver_after_disconnection) {
  ktnSignal<> signal;
  struct Receiver : public ktnSignalReceiver {
    void Slot() {}
  } receiver;

  signal.Connect(&receiver, &Receiver::Slot);
  EXPECT_EQ(receiver.Signals().size(), 1);

  signal.DisconnectReceiver(&receiver);
  EXPECT_EQ(receiver.Signals().size(), 0);
}

TEST(lifetime, signal_is_connected_to_move_constructed_or_move_assigned_receiver) {
  ktnSignal<> signal;
  class Receiver : public ktnSignalReceiver {
  public:
    Receiver() {}
    Receiver(Receiver &other) = delete;
    Receiver(Receiver &&other) : ktnSignalReceiver(std::move(other)) {
      Called = other.Called;
    }
    Receiver &operator=(Receiver &other) = delete;
    Receiver &operator=(Receiver &&other) {
      ktnSignalReceiver::operator=(std::move(other));
      Called = other.Called;
      return *this;
    }
    void operator()() {
      Called = true;
    }
    bool Called = false;
  };

  Receiver receiver;
  signal.Connect(&receiver, &Receiver::operator());

  Receiver moveConstructed{std::move(receiver)};
  EXPECT_EQ(moveConstructed.Signals().size(), 1);

  signal.Emit();
  EXPECT_FALSE(receiver.Called);
  EXPECT_TRUE(moveConstructed.Called);

  auto moveAssigned = Receiver();
  moveAssigned = std::move(moveConstructed);
  EXPECT_EQ(moveAssigned.Signals().size(), 1);

  moveConstructed.Called = false;
  signal.Emit();
  EXPECT_FALSE(moveConstructed.Called);
  EXPECT_TRUE(moveAssigned.Called);

  auto disconnectionResult = signal.DisconnectReceiver(&moveAssigned);
  EXPECT_EQ(disconnectionResult, DisconnectionResult::Success);
  EXPECT_EQ(moveAssigned.Signals().size(), 0);
}

TEST(lifetime, move_constructed_signal_is_connected_to_repeater) {
  class Receiver : public ktnSignalReceiver {
  public:
    void Slot() {
      SlotCalled = true;
    }
    bool SlotCalled = false;
  };
  Receiver receiver;
  ktnSignal signal;
  ktnSignal repeater;

  signal.Connect(&repeater);
  repeater.Connect(&receiver, &Receiver::Slot);

  ktnSignal moveConstructed = std::move(signal);
  signal.Emit();
  EXPECT_FALSE(receiver.SlotCalled);

  moveConstructed.Emit();
  EXPECT_TRUE(receiver.SlotCalled);
}

TEST(lifetime, receiver_moved_connections_to_lambda_removed) {
  class Receiver : public ktnSignalReceiver {};
  Receiver receiver;
  ktnSignal signal;

  bool slotCalled = false;
  auto lambda = [&slotCalled] { slotCalled = true; };
  std::ignore = signal.Connect(&receiver, lambda);
  signal.Emit();
  EXPECT_TRUE(slotCalled);
  slotCalled = false;

  auto moveConstructed = std::move(receiver);
  signal.Emit();
  EXPECT_FALSE(slotCalled);
}

TEST(lifetime, repeater_moved_connection_is_moved) {
  class Receiver : public ktnSignalReceiver {
  public:
    void Slot() {
      SlotCalled = true;
    }
    bool SlotCalled = false;
  };
  Receiver receiver;
  ktnSignal signal;
  ktnSignal repeater;

  signal.Connect(&repeater);
  repeater.Connect(&receiver, &Receiver::Slot);
  EXPECT_EQ(repeater.ConnectionIDs().size(), 2); // one connects to the signal, one to the receiver
  signal.Emit();
  EXPECT_TRUE(receiver.SlotCalled);
  receiver.SlotCalled = false;

  auto moveConstructed = std::move(repeater);
  EXPECT_EQ(repeater.ConnectionIDs().size(), 0);
  EXPECT_EQ(moveConstructed.ConnectionIDs().size(), 2);
  EXPECT_EQ(signal.Disconnect(&repeater), DisconnectionResult::NotNecessary);
  signal.Emit();
  EXPECT_TRUE(receiver.SlotCalled);
  receiver.SlotCalled = false;
  EXPECT_EQ(signal.Disconnect(&moveConstructed), DisconnectionResult::Success);
  signal.Emit();
  EXPECT_FALSE(receiver.SlotCalled);
}

TEST(lifetime, signal_moved_connections_is_moved) {
  class Receiver : public ktnSignalReceiver {};
  Receiver receiver;
  ktnSignal signal;

  bool slotCalled = false;
  auto lambda = [&slotCalled] { slotCalled = true; };
  std::ignore = signal.Connect(&receiver, lambda);
  EXPECT_EQ(signal.ConnectionIDs().size(), 1);
  signal.Emit();
  EXPECT_TRUE(slotCalled);
  slotCalled = false;

  ktnSignal moveConstructed(std::move(signal));
  EXPECT_EQ(signal.ConnectionIDs().size(), 0);
  EXPECT_EQ(moveConstructed.ConnectionIDs().size(), 1);
  signal.Emit();
  EXPECT_FALSE(slotCalled);
  moveConstructed.Emit();
  EXPECT_TRUE(slotCalled);
  slotCalled = false;

  ktnSignal moveAssigned;
  moveAssigned = std::move(moveConstructed);
  EXPECT_EQ(moveConstructed.ConnectionIDs().size(), 0);
  EXPECT_EQ(moveAssigned.ConnectionIDs().size(), 1);
  signal.Emit();
  EXPECT_FALSE(slotCalled);
  moveConstructed.Emit();
  EXPECT_FALSE(slotCalled);
  moveAssigned.Emit();
  EXPECT_TRUE(slotCalled);
}

TEST(lifetime_receiver_with_signal_member, deletion_ok_no_deadlock) {
  class Receiver : public ktnSignalReceiver {
  public:
    void Slot() {
      SlotCalled = true;
    }
    ktnSignal<> Signal;
    bool SlotCalled = false;
  };
  auto receiver = std::make_shared<Receiver>();
  receiver->Signal.Connect(receiver.get(), &Receiver::Slot);

  receiver.reset();

  // no EXPECT or ASSERT - the test simply must not hang
}

TEST(lifetime_receiver_with_signal_member, move_ok) {
  class Receiver : public ktnSignalReceiver {
  public:
    void Slot() {
      SlotCalled = true;
    }
    ktnSignal<> Signal;
    bool SlotCalled = false;
  };

  Receiver toBeMoved;
  toBeMoved.Signal.Connect(&toBeMoved, &Receiver::Slot);
  Receiver moveConstructed = std::move(toBeMoved);

  EXPECT_TRUE(toBeMoved.Signal.ConnectionIDs().empty());
  EXPECT_FALSE(moveConstructed.Signal.ConnectionIDs().empty());
  EXPECT_EQ(
      moveConstructed.Signal.Disconnect(&toBeMoved, &Receiver::Slot),
      DisconnectionResult::NotNecessary);
  EXPECT_EQ(
      moveConstructed.Signal.Disconnect(&moveConstructed, &Receiver::Slot),
      DisconnectionResult::Success);
}
