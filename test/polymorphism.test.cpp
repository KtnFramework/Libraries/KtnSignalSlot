/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2021-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <gtest/gtest.h>

#define KTNSIGNALSLOT_SIMPLE_TYPEDEFS
#include <ktnSignalSlot/ktnSignalSlot.hpp>

TEST(polymorphism, connections_to_overridden_functions_must_be_duplicates) {
  static constexpr char BaseSlot1Val[] = "virtual function";
  static constexpr char BaseSlot2Val[] = "non-virtual function in base";
  class Base : public ktnSignalReceiver {
  public:
    virtual void Slot1() {
      Value = BaseSlot1Val;
    };
    void Slot2() {
      Value = BaseSlot2Val;
    }
    std::string Value;
  };

  static constexpr char DerivedSlot1Val[] = "overridden virtual function";
  static constexpr char DerivedSlot2Val[] = "non-virtual function of the same name in derived";
  class Derived : public Base {
  public:
    void Slot1() final {
      Value = DerivedSlot1Val;
    };
    void Slot2() {
      Value = DerivedSlot2Val;
    }
  };

  ktnSignal<> signal;
  Derived derived;

  auto result = signal.Connect(&derived, &Base::Slot1).Result;
  EXPECT_EQ(result, ktn::SignalSlot::ConnectionResult::Success);
  result = signal.Connect(&derived, &Derived::Slot1).Result;
  EXPECT_EQ(result, ktn::SignalSlot::ConnectionResult::Duplicate);

  EXPECT_EQ(signal.ConnectionIDs().size(), 1);
  EXPECT_TRUE(derived.Value.empty());
  signal.Emit();
  EXPECT_EQ(derived.Value, DerivedSlot1Val);

  signal.DisconnectAll();
  signal.Connect(&derived, &Base::Slot2);
  derived.Value.clear();
  signal.Emit();
  EXPECT_EQ(derived.Value, BaseSlot2Val);

  signal.DisconnectAll();
  signal.Connect(&derived, &Derived::Slot2);
  derived.Value.clear();
  signal.Emit();
  EXPECT_EQ(derived.Value, DerivedSlot2Val);
}

TEST(polymorphism, connection_to_subclass_must_be_successful_without_explicit_cast) {
  class Receiver : public ktnSignalReceiver {
  public:
    void Slot() {
      Called = true;
    }
    bool Called = false;
  };

  class Derived : public Receiver {};

  ktnSignal<> signal;
  Derived derived;

  auto result = signal.Connect(&derived, &Receiver::Slot).Result;
  EXPECT_EQ(result, ktn::SignalSlot::ConnectionResult::Success);

  EXPECT_FALSE(derived.Called);
  signal.Emit();
  EXPECT_TRUE(derived.Called);
}
