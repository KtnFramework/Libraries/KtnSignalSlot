/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2019-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <gtest/gtest.h>

#include "common.hpp"

#include <ktnSignalSlot/ktnSignalSlot.hpp>

using namespace ktn::SignalSlot;

template <class C>
class connection_management : public ::testing::Test {
public:
  using SignalVoid = typename C::SignalVoid;
  using SignalSizeRef = typename C::SignalSizeRef;
};

TYPED_TEST_SUITE(connection_management, ::testing::Types<test::TestSignalTypes<ktnSignal>>);

TYPED_TEST(connection_management, repeated_connection_results_in_no_duplicate_slots) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  struct Receiver : public ktnSignalReceiver {
    void Slot() {}
  } receiver;
  auto slot = [] {};

  auto result1 = signal.template Connect<void>(slot);
  auto result2 = signal.Connect(&receiver, &Receiver::Slot);
  EXPECT_EQ(result1.ID, 1);
  EXPECT_EQ(result2.ID, 2);
  EXPECT_EQ(signal.ConnectionIDs().size(), 2);

  auto result3 = signal.template Connect<void>(slot);
  auto result4 = signal.Connect(&receiver, &Receiver::Slot);
  EXPECT_EQ(result3.Result, ConnectionResult::Duplicate);
  EXPECT_EQ(result3.ID, 1);
  EXPECT_EQ(result4.Result, ConnectionResult::Duplicate);
  EXPECT_EQ(result4.ID, 2);
  EXPECT_EQ(signal.ConnectionIDs().size(), 2);
}

TYPED_TEST(connection_management, removal_of_signal_with_many_connections_from_receiver) {
  ::testing::Test::RecordProperty(
      "Description",
      "This tests shows that removal of a signal with many connections to a receiver"
      "does not remove the signal from the receiver itself, unless there is no connection left");

  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  struct Receiver : public ktnSignalReceiver {
    void Slot1() {
      std::cout << __FUNCTION__ << std::endl;
    }
    void Slot2() {
      std::cout << __FUNCTION__ << std::endl;
    }
    void Slot3() {
      std::cout << __FUNCTION__ << std::endl;
    }
  } receiver;

  signal.Connect(&receiver, &Receiver::Slot1);
  auto id = signal.Connect(&receiver, &Receiver::Slot2).ID;
  signal.Connect(&receiver, &Receiver::Slot3);
  EXPECT_EQ(signal.ConnectionIDs().size(), 3);
  EXPECT_EQ(receiver.Signals().size(), 1);

  // disconnection does not remove signal from receiver if other slots are still connected.
  DisconnectionResult result;
  result = signal.Disconnect(&receiver, &Receiver::Slot1);
  EXPECT_EQ(result, DisconnectionResult::Success);
  EXPECT_EQ(signal.ConnectionIDs().size(), 2);
  EXPECT_EQ(receiver.Signals().size(), 1);

  result = signal.DisconnectByID(id);
  EXPECT_EQ(result, DisconnectionResult::Success);
  EXPECT_EQ(signal.ConnectionIDs().size(), 1);
  EXPECT_EQ(receiver.Signals().size(), 1);
}

TYPED_TEST(connection_management, cleanup_after_removal_of_many_receivers) {
  ::testing::Test::RecordProperty(
      "Description",
      "All connections must be removed cleanly when many receivers are destroyed.\n"
      "Previous implementations erased slots while iterating through the slot list,\n"
      "which not only was slow when a lot of slots are removed,\n"
      "but also skipped an iterator for each slot removed,\n"
      "resulting in the incomplete removal of slots whose receivers were deleted.\n"
      "This test was retroactively added to prevent regression.");

  using SignalSizeRef = typename TestFixture::SignalSizeRef;
  SignalSizeRef signal;

  const int numberOfDuplicates(100);
  size_t nRecordedCalls = 0;
  struct Receiver : public ktnSignalReceiver {
    void Slot(size_t &eNRecordedCalls) {
      ++eNRecordedCalls;
    }
  };

  Receiver receiver;
  // All slots must be deleted after removing all receivers
  {
    {
      std::vector<Receiver> receivers(numberOfDuplicates);

      for (auto &r : receivers) {
        signal.Connect(&r, &Receiver::Slot);
      }
      signal.Emit(nRecordedCalls);
      EXPECT_EQ(nRecordedCalls, numberOfDuplicates);
    }
    nRecordedCalls = 0;
    signal.Emit(nRecordedCalls);
    EXPECT_EQ(nRecordedCalls, 0);
  }

  // Removed slots must correspond to removed receivers
  {
    std::vector<std::unique_ptr<Receiver>> receivers(numberOfDuplicates);
    for (auto &r : receivers) {
      r = std::make_unique<Receiver>();
    }
    for (auto &r : receivers) {
      signal.Connect(r.get(), &Receiver::Slot);
    }
    nRecordedCalls = 0;
    signal.Emit(nRecordedCalls);
    EXPECT_EQ(nRecordedCalls, numberOfDuplicates);

    srand(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));

    std::set<size_t> tobe_removed;
    for (size_t i = 0; i < numberOfDuplicates / 2; ++i) {
      tobe_removed.insert(rand() % numberOfDuplicates);
    }
    for (auto &id : tobe_removed) {
      receivers.at(id).reset();
    }

    auto connections = signal.ConnectionIDs();
    for (size_t i = 0; i < receivers.size(); ++i) {
      if (nullptr == receivers.at(i)) {
        EXPECT_EQ(std::find(connections.begin(), connections.end(), i + 1), connections.end());
      } else {
        EXPECT_NE(std::find(connections.begin(), connections.end(), i + 1), connections.end());
      }
    }
  }
}

TYPED_TEST(connection_management, disconnection_during_emission_triggers_cleanup) {
  ::testing::Test::RecordProperty(
      "Description",
      "This test demonstrates that disconnecting from member functions during emission "
      "results in a cleanup on the receiver side "
      "if and only if the signal is not connected to any other function of the same receiver");

  using SignalVoid = typename TestFixture::SignalVoid;
  static SignalVoid signal;

  struct Receiver : ktnSignalReceiver {
    void SlotNormal() {}
    void SlotThatDisconnectsOtherSlot() {
      signal.Disconnect(this, &Receiver::SlotNormal);
    }
    void SlotThatDisconnectsItself() {
      signal.Disconnect(this, &Receiver::SlotThatDisconnectsItself);
    }
  };

  Receiver receiver;
  signal.Connect(&receiver, &Receiver::SlotNormal);
  signal.Connect(&receiver, &Receiver::SlotThatDisconnectsOtherSlot);
  EXPECT_EQ(signal.ConnectionIDs().size(), 2);
  signal.Emit();
  EXPECT_EQ(signal.ConnectionIDs().size(), 1);
  EXPECT_EQ(receiver.Signals().size(), 1);

  signal.DisconnectAll();
  signal.Connect(&receiver, &Receiver::SlotThatDisconnectsItself);
  EXPECT_EQ(signal.ConnectionIDs().size(), 1);
  signal.Emit();
  EXPECT_EQ(signal.ConnectionIDs().size(), 0);
  EXPECT_EQ(receiver.Signals().size(), 0);
}

TYPED_TEST(connection_management, connected_repeater_contains_reference_to_original_signal) {
  using SignalVoid = typename TestFixture::SignalVoid;
  SignalVoid signal;
  SignalVoid repeater;

  signal.Connect(&repeater);

  EXPECT_EQ(signal.ConnectionIDs().size(), 1);
  EXPECT_EQ(repeater.ConnectionIDs().size(), 1);
  signal.Disconnect(&repeater);

  EXPECT_EQ(signal.ConnectionIDs().size(), 0);
  EXPECT_EQ(repeater.ConnectionIDs().size(), 0);
}
