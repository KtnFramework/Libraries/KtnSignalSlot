/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2019-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <gtest/gtest.h>

#include <ktnSignalSlot/ktnSignalSlot.hpp>

using namespace ktn::SignalSlot;

template <template <typename... Args> class T>
struct EmisTestSignals {
  T<> SignalVoid;
  T<bool &> SignalBoolRef;
  T<> SignalVoidRepeater;
};

using SignalTypes = ::testing::Types<EmisTestSignals<ktnSignal>>;

template <class EmisTestSignals>
class emission : public ::testing::Test {
public:
  EmisTestSignals Signals;
};

TYPED_TEST_SUITE(emission, SignalTypes);

TYPED_TEST(emission, connected_slots_called_when_signal_emits) {
  static auto signal = &this->Signals.SignalBoolRef;
  auto slot = [](bool &called) { called = true; };
  signal->template Connect<void>(slot);

  bool called = false;
  signal->Emit(called);
  EXPECT_EQ(called, true);
}

TYPED_TEST(emission, slots_connected_via_another_slot_during_emission_is_not_called) {
  static auto signal = &this->Signals.SignalVoid;
  static auto repeater = &this->Signals.SignalVoidRepeater;

  static size_t callCount = 0;
  auto lambdaSlot = [&]() { ++callCount; };
  auto freeSlot = [] { ++callCount; };
  struct Receiver : public ktnSignalReceiver {
    void Slot() {
      ++callCount;
    };
  } receiver;
  std::ignore = repeater->Connect(&receiver, [] { ++callCount; });

  std::ignore = signal->Connect(&receiver, [&] {
    std::ignore = signal->Connect(&receiver, lambdaSlot);
    signal->template Connect<void>(freeSlot);
    signal->Connect(&receiver, &Receiver::Slot);
    signal->Connect(repeater);
  });

  signal->Emit();

  EXPECT_EQ(callCount, 0);
}

void freeFunc(bool &called) {
  called = true;
}

TYPED_TEST(emission, no_calling_of_functions_disconnected_while_emitting_using_DisconnectAll) {
  static auto signal = &this->Signals.SignalBoolRef;

  auto slot_DisconnectAll = [](bool &) { signal->DisconnectAll(); };
  signal->template Connect<void>(slot_DisconnectAll);
  signal->Connect(&freeFunc);
  EXPECT_EQ(signal->ConnectionIDs().size(), 2);
  bool called = false;
  signal->Emit(called);
  EXPECT_EQ(signal->ConnectionIDs().size(), 0);
  EXPECT_FALSE(called);
}

TYPED_TEST(emission, no_calling_of_functions_disconnected_while_emitting_using_DisconnectByID) {
  static auto signal = &this->Signals.SignalBoolRef;
  static DisconnectionResult result;
  static size_t id = 0;
  static bool called = 0;
  auto slot_DisconnectByID = [](bool &) { result = signal->DisconnectByID(id); };
  signal->template Connect<void>(slot_DisconnectByID);
  id = signal->Connect(&freeFunc).ID;
  signal->Emit(called);
  EXPECT_FALSE(called);

  // disconnection of non-connected slot is unnecessary
  signal->template Connect<void>(slot_DisconnectByID);
  auto connections = signal->ConnectionIDs();

  // Set id to an ID not used by any connection.
  // When slot_DisconnectByID is called, it will try to disconnect by this ID,
  // which should return DisconnectionResult::NotNecessary
  id = *std::max_element(connections.begin(), connections.end()) + 1;
  signal->Emit(called);
  EXPECT_EQ(result, DisconnectionResult::NotNecessary);
}

TYPED_TEST(
    emission,
    no_calling_of_functions_disconnected_while_emitting_using_Disconnect___free_function) {
  static auto signal = &this->Signals.SignalBoolRef;
  static DisconnectionResult result = DisconnectionResult::Unknown;
  static bool called = false;

  auto slot_DisconnectByName = [](bool &) { result = signal->Disconnect(&freeFunc); };
  signal->template Connect<void>(slot_DisconnectByName);
  signal->Connect(&freeFunc);
  signal->Emit(called);
  EXPECT_FALSE(called);

  // disconnection of non-connected slot is unnecessary
  signal->template Connect<void>(slot_DisconnectByName);
  signal->Emit(called);
  EXPECT_EQ(result, DisconnectionResult::NotNecessary);
}

TYPED_TEST(
    emission,
    no_calling_of_functions_disconnected_while_emitting_using_Disconnect___member_function) {
  static auto signal = &this->Signals.SignalVoid;

  struct NestedDisconnections : public ktnSignalReceiver {
    struct UnusedReceiver : public ktnSignalReceiver {

    } Receiver;

    void Reset() {
      signal->DisconnectAll();
      SlotCalled = false;
      DiscoResult_Slot = DisconnectionResult::Unknown;
      DiscoResult_Unused = DisconnectionResult::Unknown;
    }

    void DisconnectMemberFunctionByName() {
      DiscoResult_Slot = signal->Disconnect(this, &NestedDisconnections::Slot);
    }

    void Slot() {
      SlotCalled = true;
    }

    void DisconnectSelf() {
      signal->DisconnectReceiver(this);
    }

    void DisconnectUnusedReceiver() {
      DiscoResult_Unused = signal->DisconnectReceiver(&Receiver);
    }

    DisconnectionResult DiscoResult_Slot = DisconnectionResult::Unknown;
    DisconnectionResult DiscoResult_Unused = DisconnectionResult::Unknown;

    bool SlotCalled = false;
  };

  NestedDisconnections nestedDisconnections;

  nestedDisconnections.Reset();
  signal->Connect(&nestedDisconnections, &NestedDisconnections::DisconnectMemberFunctionByName);
  signal->Connect(&nestedDisconnections, &NestedDisconnections::Slot);
  EXPECT_EQ(signal->ConnectionIDs().size(), 2);

  // during this signal emission, Slot is removed
  signal->Emit();
  EXPECT_EQ(signal->ConnectionIDs().size(), 1);
  EXPECT_EQ(nestedDisconnections.SlotCalled, false);

  // disconnection of non-connected slot is unnecessary and reported as such
  nestedDisconnections.Reset();
  signal->Connect(&nestedDisconnections, &NestedDisconnections::DisconnectMemberFunctionByName);
  EXPECT_EQ(nestedDisconnections.DiscoResult_Slot, DisconnectionResult::Unknown);
  signal->Emit();
  EXPECT_EQ(nestedDisconnections.DiscoResult_Slot, DisconnectionResult::NotNecessary);
}

TYPED_TEST(emission, no_calling_of_functions_disconnected_while_emitting_using_DisconnectReceiver) {
  static auto signal = &this->Signals.SignalVoid;

  struct NestedDisconnections : public ktnSignalReceiver {
    struct UnusedReceiver : public ktnSignalReceiver {

    } Receiver;

    void Reset() {
      signal->DisconnectAll();
      SlotCalled = false;
      DiscoResult_Slot = DisconnectionResult::Unknown;
      DiscoResult_Unused = DisconnectionResult::Unknown;
    }

    void DisconnectMemberFunctionByName() {
      DiscoResult_Slot = signal->Disconnect(this, &NestedDisconnections::Slot);
    }

    void Slot() {
      SlotCalled = true;
    }

    void DisconnectSelf() {
      signal->DisconnectReceiver(this);
    }

    void DisconnectUnusedReceiver() {
      DiscoResult_Unused = signal->DisconnectReceiver(&Receiver);
    }

    DisconnectionResult DiscoResult_Slot = DisconnectionResult::Unknown;
    DisconnectionResult DiscoResult_Unused = DisconnectionResult::Unknown;

    bool SlotCalled = false;
  };

  NestedDisconnections nestedDisconnections;
  nestedDisconnections.Reset();
  signal->Connect(&nestedDisconnections, &NestedDisconnections::DisconnectSelf);
  signal->Connect(&nestedDisconnections, &NestedDisconnections::DisconnectMemberFunctionByName);
  signal->Connect(&nestedDisconnections, &NestedDisconnections::Slot);
  EXPECT_EQ(signal->ConnectionIDs().size(), 3);

  signal->Emit();
  EXPECT_EQ(signal->ConnectionIDs().size(), 0);
  EXPECT_EQ(nestedDisconnections.SlotCalled, false);

  // disconnection by unconnected receiver results in no change and is reported as not necessary
  {
    nestedDisconnections.Reset();
    signal->Connect(&nestedDisconnections, &NestedDisconnections::DisconnectUnusedReceiver);
    EXPECT_EQ(signal->ConnectionIDs().size(), 1);
    EXPECT_EQ(nestedDisconnections.DiscoResult_Unused, DisconnectionResult::Unknown);
    signal->Emit();
    EXPECT_EQ(nestedDisconnections.DiscoResult_Unused, DisconnectionResult::NotNecessary);
  }
}

TYPED_TEST(emission, no_emission_while_emitting) {
  ::testing::Test::RecordProperty(
      "Description",
      "This test ensures that a signal cannot be emitted while it is already emitting.");
  static auto signal = &this->Signals.SignalVoid;
  struct RecursiveEmitter : ktnSignalReceiver {
    void EmitSignal() {
      ++EmissionCount;
      signal->Emit();
    }
    size_t EmissionCount = 0;
  };

  RecursiveEmitter emitter;
  signal->Connect(&emitter, &RecursiveEmitter::EmitSignal);
  EXPECT_EQ(emitter.EmissionCount, 0);
  signal->Emit();
  EXPECT_EQ(emitter.EmissionCount, 1);
}
