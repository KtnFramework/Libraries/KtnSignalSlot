/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2020-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <benchmark/benchmark.h>

#include "ktnSignalSlot.bench.common.hpp"

template <class Signal>
void Constructor(benchmark::State &state) {
  using namespace std::chrono;
  for (auto _ : state) {
    auto start = high_resolution_clock::now();
    Signal s;
    state.SetIterationTime(
        duration_cast<duration<double>>(high_resolution_clock::now() - start).count());
  }
}

template <class Signal>
void Destructor(benchmark::State &state) {
  using namespace std::chrono;
  high_resolution_clock::time_point start;
  for (auto _ : state) {
    {
      Signal s;
      start = high_resolution_clock::now();
    }
    state.SetIterationTime(
        duration_cast<duration<double>>(high_resolution_clock::now() - start).count());
  }
}

BENCHMARK_TEMPLATE(Constructor, ktn::SignalSlot::ktnSignal<>)->UseManualTime();
BENCHMARK_TEMPLATE(Destructor, ktn::SignalSlot::ktnSignal<>)->UseManualTime();
