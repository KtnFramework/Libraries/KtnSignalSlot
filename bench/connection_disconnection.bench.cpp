/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2020-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <benchmark/benchmark.h>

#include "ktnSignalSlot.bench.common.hpp"

void slot() {}

template <class Signal>
void Connect_FreeFunction(benchmark::State &state) {
  using namespace std::chrono;
  Signal signal;
  high_resolution_clock::time_point start;
  auto slot = []() {};

  for (auto _ : state) {
    start = high_resolution_clock::now();
    signal.template Connect<void>(slot);
    state.SetIterationTime(
        duration_cast<duration<double>>(high_resolution_clock::now() - start).count());
    signal.template Disconnect<void>(slot);
  }
}

template <class Signal, class Receiver>
void Connect_MemberFunction(benchmark::State &state) {
  using namespace std::chrono;
  Signal signal;
  std::vector<Receiver *> receivers;
  for (int i = 0; i < state.range(0); ++i) {
    receivers.emplace_back(new Receiver());
  }
  high_resolution_clock::time_point start;

  for (auto _ : state) {
    start = high_resolution_clock::now();
    for (auto &receiver : receivers) {
      signal.Connect(receiver, &Receiver::Slot);
    }

    state.SetIterationTime(
        duration_cast<duration<double>>(high_resolution_clock::now() - start).count());

    for (auto &receiver : receivers) {
      signal.Disconnect(receiver, &Receiver::Slot);
    }
  }
  for (auto &receiver : receivers) delete receiver;
  receivers.clear();
}

template <class Signal>
void Disconnect_FreeFunction(benchmark::State &state) {
  using namespace std::chrono;
  Signal signal;
  high_resolution_clock::time_point start;
  auto slot = []() {};

  for (auto _ : state) {
    signal.template Connect<void>(slot);
    start = high_resolution_clock::now();
    signal.template Disconnect<void>(slot);
    state.SetIterationTime(
        duration_cast<duration<double>>(high_resolution_clock::now() - start).count());
  }
}

BENCHMARK_TEMPLATE(Connect_FreeFunction, ktn::SignalSlot::ktnSignal<>)->UseManualTime();
BENCHMARK_TEMPLATE(Connect_MemberFunction, ktn::SignalSlot::ktnSignal<>, receiver)
    ->Range(2, 256)
    ->UseManualTime();
BENCHMARK_TEMPLATE(Disconnect_FreeFunction, ktn::SignalSlot::ktnSignal<>)->UseManualTime();
