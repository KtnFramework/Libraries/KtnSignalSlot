/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2020-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <benchmark/benchmark.h>

#include "ktnSignalSlot.bench.common.hpp"

template <class Signal, class Receiver>
void emission(benchmark::State &state) {
  using namespace std::chrono;
  Signal signal;
  std::vector<Receiver> receivers;
  for (int i = 0; i < state.range(0); ++i) {
    receivers.emplace_back(Receiver());
    signal.Connect(&receivers.at(i), &Receiver::Slot);
  }

  for (auto _ : state) {
    signal.Emit();
  }

  receivers.clear();
}

template <class Signal, class Receiver>
class emission_threaded : public benchmark::Fixture {
protected:
  void SetUp(const ::benchmark::State &state) {
    auto guard = std::lock_guard(m_Mutex);
    for (int i = 0; i < state.range(0); ++i) {
      receivers.emplace_back(new Receiver);
    }
    for (auto &receiver : receivers) {
      signal.Connect(receiver, &Receiver::Slot);
    }
  }

  Signal signal;
  std::vector<Receiver *> receivers;

  void TearDown(const ::benchmark::State & /*state*/) {
    auto guard = std::lock_guard(m_Mutex);
    signal.DisconnectAll();
    for (auto &receiver : receivers) delete receiver;
    receivers.clear();
  }

private:
  std::mutex m_Mutex;
};

BENCHMARK_TEMPLATE(emission, ktn::SignalSlot::ktnSignal<>, receiver)->Range(8, 1024);

BENCHMARK_TEMPLATE_DEFINE_F(emission_threaded, ktnSignal, ktn::SignalSlot::ktnSignal<>, receiver)
(benchmark::State &st) {
  for (auto _ : st) {
    signal.Emit();
  }
}

BENCHMARK_REGISTER_F(emission_threaded, ktnSignal)
    ->RangeMultiplier(2)
    ->ThreadRange(2, 8)
    ->Range(4, 16)
    ->Repetitions(20)
    ->DisplayAggregatesOnly();
