/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2020-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <benchmark/benchmark.h>

#include "ktnSignalSlot.bench.common.hpp"

template <class Signal>
void SignalSize(benchmark::State &state) {
  for (auto _ : state) {
    state.counters["Signal Size"] = sizeof(Signal);
  }
}

template <class Receiver>
void ReceiverSize(benchmark::State &state) {
  for (auto _ : state) {
    state.counters["Receiver Size"] = sizeof(Receiver);
  }
}

BENCHMARK_TEMPLATE(SignalSize, ktn::SignalSlot::ktnSignal<>)->Iterations(1);
BENCHMARK_TEMPLATE(ReceiverSize, ktn::SignalSlot::ktnSignalReceiver)->Iterations(1);
