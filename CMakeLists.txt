################################################################################
# Copyright 2019-2024 Tien Dat Nguyen
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

cmake_minimum_required(VERSION 3.10)
project(KtnSignalSlot)

option(KTNSIGNALSLOT_BUILD_BENCHMARKS "Build benchmarks." FALSE)
option(KTNSIGNALSLOT_BUILD_EXAMPLES "Build examples." FALSE)
option(KTNSIGNALSLOT_BUILD_TESTS "Build tests." FALSE)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(KtnSignalSlot
    INTERFACE
    include/ktnSignalSlot/ktnSignalSlot.hpp
)
add_library(ktn::SignalSlot ALIAS KtnSignalSlot)
target_include_directories(KtnSignalSlot INTERFACE include/)

if(KTNSIGNALSLOT_BUILD_EXAMPLES)
    add_subdirectory(examples)
endif()

if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
    set(gtest_force_shared_crt ON CACHE BOOL "Use shared (DLL) run-time lib even when Google Test is built as static lib." FORCE)
endif()

if(KTNSIGNALSLOT_BUILD_TESTS OR KTNSIGNALSLOT_BUILD_BENCHMARKS)
    add_subdirectory(external/google/googletest)
endif()

if(KTNSIGNALSLOT_BUILD_TESTS)
    add_subdirectory(test)
endif()

if(KTNSIGNALSLOT_BUILD_BENCHMARKS)
    set(BENCHMARK_ENABLE_TESTING OFF CACHE BOOL "Suppressing benchmark's tests" FORCE)
    add_subdirectory(external/google/benchmark)
    add_subdirectory(bench)
endif()

add_custom_target(KtnSignalSlot_Maintenance
    SOURCES
    .clang-format
)
