#!/bin/bash

################################################################################
# Copyright 2019-2022 Tien Dat Nguyen
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

UNIT_TEST_DIR="build/test"
UNIT_TEST_FILE="KtnSignalSlot-unit-test"

if [ ! -d $UNIT_TEST_DIR ]; then
    echo "Could not find directory \"$UNIT_TEST_DIR\"."
    exit 1
else
    cd $UNIT_TEST_DIR

    if [ -f $UNIT_TEST_FILE ];then
        ./$UNIT_TEST_FILE
    else
        echo "Could not find file \"$UNIT_TEST_DIR/$UNIT_TEST_FILE\"."
        cd ../..
        exit 1
    fi

    gcovr -r ../.. \
    --exclude ../../example/ \
    --exclude ../../external/ \
    --exclude ../../test/ --html-details -o coverage.html || { exit 1; }

    gcovr -r ../.. \
    --exclude ../../example/ \
    --exclude ../../external/ \
    --exclude ../../test/ || { exit 1; }
    cd ../..
fi
