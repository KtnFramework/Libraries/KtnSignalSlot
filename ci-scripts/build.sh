#!/bin/bash

################################################################################
# Copyright 2019-2022 Tien Dat Nguyen
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

# parse arguments
KTNSIGNALSLOT_BENCHMARKS_TOGGLE=OFF
KTNSIGNALSLOT_EXAMPLES_TOGGLE=OFF
KTNSIGNALSLOT_TESTS_TOGGLE=OFF
KTNSIGNALSLOT_FORCE_REBUILD=FALSE
for i in "$@"; do
    case $i in
    --build-benchmarks)
        KTNSIGNALSLOT_BENCHMARKS_TOGGLE=ON
        ;;
    --build-examples)
        KTNSIGNALSLOT_EXAMPLES_TOGGLE=ON
        ;;
    --build-unit-tests)
        KTNSIGNALSLOT_TESTS_TOGGLE=ON
        ;;
    -f|--force-rebuild)
        KTNSIGNALSLOT_FORCE_REBUILD=TRUE
    esac
done



if [ -d build ]; then
    if [ "$KTNSIGNALSLOT_FORCE_REBUILD" = TRUE ]; then
        rm -rf build/
        mkdir build
    fi
else
    mkdir build
fi

cd build

cmake \
-DCMAKE_BUILD_TYPE=Release \
-DKTNSIGNALSLOT_BUILD_BENCHMARKS=$KTNSIGNALSLOT_BENCHMARKS_TOGGLE \
-DKTNSIGNALSLOT_BUILD_EXAMPLES=$KTNSIGNALSLOT_EXAMPLES_TOGGLE \
-DKTNSIGNALSLOT_BUILD_TESTS=$KTNSIGNALSLOT_TESTS_TOGGLE .. || {
    exit 1
}

make -j$(nproc) || { exit 1; }
