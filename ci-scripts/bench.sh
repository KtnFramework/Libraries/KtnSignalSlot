#!/bin/bash

################################################################################
# Copyright 2020-2022 Tien Dat Nguyen
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

BENCH_DIR="build/bench"
BENCH_FILE="KtnSignalSlot-benchmarks"

if [ ! -d $BENCH_DIR ]; then
    echo "Could not find directory \"$BENCH_DIR\"."
    exit 1
else
    cd $BENCH_DIR
    
    if [ -f $BENCH_FILE ];then
        ./$BENCH_FILE
    else
        echo "Could not find file \"$BENCH_DIR/$BENCH_FILE\"."
        cd ../..
        exit 1
    fi
    cd ../..
fi
