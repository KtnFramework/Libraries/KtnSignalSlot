/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2019-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <ktnSignalSlot/ktnSignalSlot.hpp>

#include <iostream>
#include <memory>

using namespace std;

using ktn::SignalSlot::ktnSignal;

const static inline char *EXAMPLE_STRING( //
    "This example shows how one can connect a signal to a class member function.");
const static inline char *DASHY( //
    "--------------------------------------------------------------------------------");

class Receiver : public ktn::SignalSlot::ktnSignalReceiver {
public:
  explicit Receiver(const string &eID) : m_ID(eID) {}

  void PrintString() {
    cout << "Receiver " << m_ID << ": " << EXAMPLE_STRING << endl;
  }

  unsigned int IncrementCounter() {
    cout << "Receiver " << m_ID << ": " << ++m_Counter << endl;
    return m_Counter;
  }

private:
  const string m_ID;
  unsigned int m_Counter = 0;
};

auto main() -> int {
  ktnSignal<> signal;
  auto receiver = make_unique<Receiver>("1");
  Receiver another_receiver("2");

  {
    cout << DASHY << endl;
    cout << "no receiver, nothing happens" << endl;
    signal.Emit();
  }

  {
    cout << DASHY << endl;
    cout << "1 signal, 1 receiver, 1 slot" << endl;
    signal.Connect(receiver.get(), &Receiver::PrintString);
    signal.Emit(); // the string is printed

    signal.Disconnect(receiver.get(), &Receiver::PrintString);
  }

  {
    cout << DASHY << endl;
    cout << "1 signal, multiple slots of the same receiver" << endl;
    signal.Connect(receiver.get(), &Receiver::PrintString);
    signal.Connect(receiver.get(), &Receiver::IncrementCounter);
    signal.Emit(); // the string and the counter are printed

    signal.Disconnect(receiver.get(), &Receiver::PrintString);
    signal.Disconnect(receiver.get(), &Receiver::IncrementCounter);
  }

  {
    cout << DASHY << endl;
    cout << "1 signal, multiple receivers" << endl;
    signal.Connect(receiver.get(), &Receiver::PrintString);
    signal.Connect(receiver.get(), &Receiver::IncrementCounter);
    signal.Connect(&another_receiver, &Receiver::IncrementCounter);
    signal.Emit(); // the string and the counter for each objects are printed

    signal.Disconnect(receiver.get(), &Receiver::PrintString);
    signal.Disconnect(receiver.get(), &Receiver::IncrementCounter);
    signal.Disconnect(&another_receiver, &Receiver::IncrementCounter);
  }

  {
    cout << DASHY << endl;
    cout << "no receiver, nothing happens" << endl;
    signal.Connect(receiver.get(), &Receiver::PrintString);
    receiver.reset();
    signal.Emit(); // nothing happens because all slots are disconnected
  }
  return 0;
}
