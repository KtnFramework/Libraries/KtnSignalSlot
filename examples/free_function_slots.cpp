/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2019-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <ktnSignalSlot/ktnSignalSlot.hpp>

#include <iostream>

using namespace std;

using ktn::SignalSlot::ktnSignal;

const static inline char *EXAMPLE_STRING( //
    "This example shows how one can connect a signal to a free function.");
const static inline char *DASHY( //
    "--------------------------------------------------------------------------------");

void slot1() {
  cout << __func__ << ": " << EXAMPLE_STRING << endl;
}

void slot2(int eInput) {
  cout << __func__ << ": " << eInput << endl;
}

int slot3(int eInput) {
  int output = 2 * eInput;
  cout << __func__ << ": " << output << endl;
  return output;
}

auto main() -> int {
  ktnSignal<> signal1;
  ktnSignal<int> signal2;

  {
    cout << DASHY << endl;
    cout << "No slot connected, nothing happens" << endl;
    signal1.Emit();
    signal2.Emit(0);
  }

  {
    cout << DASHY << endl;
    cout << "A signal can be connected with a free function:" << endl;
    signal1.Connect(&slot1);
    signal1.Emit();
  }

  {
    cout << DASHY << endl;
    cout << "A signal can be connected with multiple functions "
            "and called multiple times:"
         << endl;
    signal2.Connect(&slot2);
    signal2.Connect(&slot3);

    cout << string(DASHY).substr(0, 20) << endl;
    signal2.Emit(1721979);

    cout << string(DASHY).substr(0, 20) << endl;
    signal2.Emit(1431988);
  }

  {
    cout << DASHY << endl;
    cout << "Signals can be disconnected." << endl;
    signal1.Disconnect(&slot1);
    signal2.Disconnect(&slot2);
    signal2.Disconnect(&slot3);

    cout << "Once disconnected, emissions have no effects:" << endl;
    signal1.Emit();
    signal2.Emit(461989);
  }

  {
    cout << DASHY << endl;
    cout << "A signal can be defined with arbitrary many parameters." << endl;
    cout << string(DASHY).substr(0, 20) << endl;
    ktnSignal<int, float &, const string &> signal3;
    auto slot4 = [](int param1, float &param2, const string &param3) {
      cout << __func__ << ": parameters: " << param1 << " " << param2 << " " << param3 << endl;
      param2 = 1.5F;
    };

    signal3.Connect<void>(slot4);
    float f = 0.2F;
    cout << "Before signal emission: f = " << f << endl;
    signal3.Emit(1, f, "word");
    cout << "References given to a slot can be changed too, "
            "just like any normal function."
         << endl;
    cout << "After signal emission: f = " << f << endl;
  }

  return 0;
}
