﻿/*******************************************************************************
 * This file is part of the KtnSignalSlot project.
 *
 * Copyright 2019-2024 Tien Dat Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#ifndef KTNSIGNALSLOT_KTNSIGNALSLOT_HPP
#define KTNSIGNALSLOT_KTNSIGNALSLOT_HPP
#include <algorithm>
#include <atomic>
#include <functional>
#include <mutex>
#include <set>
#include <vector>

namespace ktn::SignalSlot {
template <typename... Args>
class ktnSignal;
class ktnSignalReceiver;

template <typename Base, typename ToBeChecked>
using IsBased = typename std::enable_if_t<std::is_base_of_v<Base, ToBeChecked>>;

class Base {
  template <typename... Args>
  friend class ::ktn::SignalSlot::ktnSignal;
  friend class ::ktn::SignalSlot::ktnSignalReceiver;

public:
  virtual ~Base() = default;

protected:
  virtual void _AddSignal(Base *eSignal) = 0;

  /**
   * @brief _Dissociate removes reference to another @ref ktn::SignalSlot::Base object.
   * @param target is the object from which this is to be dissociated.
   * For receivers, target is a signal. For signals, target might be another signal
   * of the same type, or a receiver.
   *
   * @details This function doesn't do any extra checks,
   * it just removes all reference of a particular associate
   * from the list of connected associates.
   * This means it can be called on object 2 from object 1,
   * while object 1 is removing reference to object 2 via other means,
   * so that it won't be called recursively and create a deadlock.
   */
  virtual void _Dissociate(Base *target) = 0;
  virtual void _HandleAssociateMoved(Base *from, Base *to) = 0;
};

class ktnSignalReceiver : public Base {
public:
  ktnSignalReceiver() = default;

  /**
   * @brief The copy constructor is only there to prevent implicit deletion
   * due to mutex' deleted copy constructor.
   */
  ktnSignalReceiver(const ktnSignalReceiver &) {}

  ktnSignalReceiver(ktnSignalReceiver &&other) {
    auto guardOther = std::lock_guard(other.m_Mutex_Signals);
    this->m_Signals = std::move(other.m_Signals);
    auto guardThis = std::lock_guard(m_Mutex_Signals);
    for (auto &signal : m_Signals) {
      signal->_HandleAssociateMoved(&other, this);
    }
  }

  ktnSignalReceiver &operator=(ktnSignalReceiver &) {
    return *this;
  }

  ktnSignalReceiver &operator=(ktnSignalReceiver &&other) {
    auto guardOther = std::lock_guard(other.m_Mutex_Signals);
    this->m_Signals = std::move(other.m_Signals);
    auto guardThis = std::lock_guard(m_Mutex_Signals);
    for (auto &signal : m_Signals) {
      signal->_HandleAssociateMoved(&other, this);
    }
    return *this;
  }

  virtual ~ktnSignalReceiver() noexcept {
    auto guard = std::lock_guard(m_Mutex_Signals);
    for (auto &signal : m_Signals) {
      signal->_Dissociate(this);
    }
  }

  const std::vector<Base *> Signals() const {
    auto guard = std::lock_guard(m_Mutex_Signals);
    return m_Signals;
  }

protected:
  void _AddSignal(Base *eSignal) final {
    auto guard = std::lock_guard(m_Mutex_Signals);
    auto iterator(std::find(m_Signals.begin(), m_Signals.end(), eSignal));
    if (iterator == m_Signals.end()) {
      m_Signals.emplace_back(eSignal);
    }
  }

  void _Dissociate(Base *target) final {
    auto guard = std::lock_guard(m_Mutex_Signals);
    auto iterator(std::find(m_Signals.begin(), m_Signals.end(), target));
    if (iterator != m_Signals.end()) {
      m_Signals.erase(iterator);
    }
  }

  void _HandleAssociateMoved(Base *from, Base *to) {
    _Dissociate(from);
    _AddSignal(to);
  }

private:
  mutable std::mutex m_Mutex_Signals;
  std::vector<Base *> m_Signals;
};

/// @brief Possible results for the Connect functions.
enum class ConnectionResult : uint_fast8_t { //
  Unknown = std::numeric_limits<uint_fast8_t>::max(), //!< Default value.
  Duplicate = 2, //!< Connection already exists.
  Failure = 1, //!< Connection does not exists, no new connection was created.
  Success = 0 //!< New connection was created.
};

struct ConnectionResultWithId {
  /**
   * @brief The constructor.
   * Note that since ID and Result are const,
   * no other constructor is provided.
   * @param eID
   * @param eResult
   */
  ConnectionResultWithId(size_t eID, ConnectionResult eResult) : ID(eID), Result(eResult) {}

  /**
   * @brief The ID of the resulting connection.
   * Is 0 when no connection is created.
   */
  const size_t ID;

  /**
   * @brief Result
   * @see ConnectionResult
   */
  const ConnectionResult Result;
};

/// @brief Possible results for the Disconnect functions.
enum class DisconnectionResult : uint_fast8_t {
  //! Default value.
  Unknown = std::numeric_limits<uint_fast8_t>::max(),

  //! Removal was not neccessary.
  //! Either the connection doesn't exist, or is being removed.
  NotNecessary = 1,

  //! The slot was removed.
  Success = 0
};

/// @brief The ktnSignal class is the template class for all signals.
template <typename... Args>
class ktnSignal final : public Base {
public:
  ktnSignal() = default; //!< The default constructor.
  ktnSignal(ktnSignal &) {};

  ktnSignal(ktnSignal &&other) {
    auto guardOther_Connections = std::lock_guard(other.m_Mutex_Connections);
    auto guardOther_Slots = std::lock_guard(other.m_Mutex_Slots);
    m_Connections = std::move(other.m_Connections);
    auto guardThis_Connections = std::lock_guard(m_Mutex_Connections);
    auto guardThis_Slots = std::lock_guard(m_Mutex_Slots);
    for (auto &connection : m_Connections) {
      if (connection.Slot->Context() != nullptr) {
        connection.Slot->Context()->_HandleAssociateMoved(&other, this);
      }
    }
  };

  ktnSignal &operator=(ktnSignal &&other) {
    if (this == &other) {
      return *this;
    }

    DisconnectAll();
    auto guardOther_Connections = std::lock_guard(other.m_Mutex_Connections);
    auto guardOther_Slots = std::lock_guard(other.m_Mutex_Slots);
    m_Connections = std::move(other.m_Connections);
    auto guardThis_Connections = std::lock_guard(m_Mutex_Connections);
    auto guardThis_Slots = std::lock_guard(m_Mutex_Slots);
    for (auto &connection : m_Connections) {
      if (connection.Slot->Context() != nullptr) {
        connection.Slot->Context()->_HandleAssociateMoved(&other, this);
      }
    }

    return *this;
  }

  /**
   * @brief The destructor.
   * Some cleanup is done to remove references to this object in all receivers.
   */
  ~ktnSignal() noexcept final {
    auto guard_Connections = std::lock_guard(m_Mutex_Connections);
    auto guard_Slots = std::lock_guard(m_Mutex_Slots);
    m_Destroying = true;

    for (auto &connection : m_Connections) {
      if (connection.Slot->Context() != nullptr) { //
        connection.Slot->Context()->_Dissociate(this);
      }
      delete connection.Slot;
    }
    m_Connections.clear();
  }

  /**
   * @brief Connect connects to a member function of class C's instance eInstance.
   * @param eInstance is the instance of class C which is connected to this signal.
   * @param eFunction is the function in class C that is used as the slot.
   * @return The connection result and ID.
   *
   * @remark C must be derived from ktnSignalReceiver, and
   * eInstance can be an instance of a type that is derived from C.
   */
  template <
      class C,
      class C_Derived,
      typename N,
      class = IsBased<ktnSignalReceiver, C>,
      class = IsBased<C, C_Derived>>
  ConnectionResultWithId Connect(C_Derived *eInstance, N (C::*eFunction)(Args...)) {
    return _Connect_MemberFunction(eInstance, (N(C_Derived::*)(Args...))(eFunction));
  }

  /**
   * @brief Connect connects to a const member function of class C's instance eInstance.
   * @param eInstance is the instance of class C which is connected to this signal.
   * @param eFunction is the function in class C that is used as the slot.
   * @return The connection result and ID.
   *
   * @remark C must be derived from ktnSignalReceiver, and
   * eInstance can be an instance of a type that is derived from C.
   */
  template <
      class C,
      class C_Derived,
      typename N,
      class = IsBased<ktnSignalReceiver, C>,
      class = IsBased<C, C_Derived>>
  ConnectionResultWithId Connect(C_Derived *eInstance, N (C::*eFunction)(Args...) const) {
    return _Connect_MemberFunction(eInstance, (N(C_Derived::*)(Args...))(eFunction));
  }

  /**
   * @brief Connect connects to another signal of the same type.
   * The connected signal acts as a repeater, which emits whenever this signal emits.
   * @param eSignal is the repeater.
   * @return The connection result and ID.
   */
  ConnectionResultWithId Connect(ktnSignal *eSignal) {
    return _Connect_Signal<true>(eSignal);
  }

  /**
   * @brief Connect connects to a lambda expression.
   * @param eContext an object of a type that is derived from @ref ktnSignalReceiver.
   * When (if) this object is deleted, the slot will be removed.
   * @param eLambda is a lambda expression with the same parameter types as the signal.
   * @return The connection result and ID.
   */
  template <typename Lambda>
  [[nodiscard]] ConnectionResultWithId Connect(ktnSignalReceiver *eContext, Lambda eLambda) {
    if (m_Destroying) {
      return {0, ConnectionResult::Failure};
    };
    auto tempSlot = new ktnSlot_Lambda(eContext, eLambda);

    if (m_CallingCurrentSlots) {
      auto guard_Slots = std::lock_guard(m_Mutex_Slots);
      return _Connect_DuringEmission_NoLock<true>(tempSlot);
    }

    auto guard_Connections = std::lock_guard(m_Mutex_Connections);
    auto guard_Slots = std::lock_guard(m_Mutex_Slots);
    return _Connect_WhileIdle_NoLock<true>(tempSlot);
  }

  /**
   * @brief Connect connects to a free function or a stateless lambda.
   * @param eFunction is the function that is used as the slot.
   * @return The connection result and ID.
   *
   * @remark The connection to a lambda can only be removed by using the returned ID.
   */
  template <typename N>
  ConnectionResultWithId Connect(N (*eFunction)(Args...)) {
    if (m_Destroying) {
      return {0, ConnectionResult::Failure};
    };
    auto tempSlot = new ktnSlot_FreeFunction<N>(eFunction);

    if (m_CallingCurrentSlots) {
      auto guard_Slots = std::lock_guard(m_Mutex_Slots);
      return _Connect_DuringEmission_NoLock<false>(tempSlot);
    }

    auto guard_Connections = std::lock_guard(m_Mutex_Connections);
    auto guard_Slots = std::lock_guard(m_Mutex_Slots);
    return _Connect_WhileIdle_NoLock<false>(tempSlot);
  }

  /**
   * @brief ConnectionIDs
   * @return A set of current connections' IDs.
   */
  [[nodiscard]] std::set<size_t> ConnectionIDs() const {
    std::lock_guard guard(m_Mutex_Connections);
    return _ConnectionIDs_NoLock();
  }

  /**
   * @brief Disconnect disconnects from a member function of class C's instance eInstance.
   * @param eInstance is the instance of class C which is connected to this signal.
   * @param eFunction is the function in class C that will be disconnected.
   *
   * @remark C must be derived from ktnSignalReceiver, and
   * eInstance can be an instance of a type that is derived from C.
   */
  template <
      class C,
      class C_Derived,
      typename N,
      class = IsBased<ktnSignalReceiver, C>,
      class = IsBased<C, C_Derived>>
  DisconnectionResult Disconnect(C_Derived *eInstance, N (C::*eFunction)(Args...)) {
    return _Disconnect_MemberFunction(eInstance, (N(C_Derived::*)(Args...))(eFunction));
  }

  /**
   * @brief Disconnect disconnects from a const member function of class C's instance eInstance.
   * @param eInstance is the instance of class C which is connected to this signal.
   * @param eFunction is the function in class C that will be disconnected.
   *
   * @remark C must be derived from ktnSignalReceiver, and
   * eInstance can be an instance of a type that is derived from C.
   */
  template <
      class C,
      class C_Derived,
      typename N,
      class = IsBased<ktnSignalReceiver, C>,
      class = IsBased<C, C_Derived>>
  DisconnectionResult Disconnect(C_Derived *eInstance, N (C::*eFunction)(Args...) const) {
    return _Disconnect_MemberFunction(eInstance, (N(C_Derived::*)(Args...))(eFunction));
  }

  DisconnectionResult Disconnect(ktnSignal *eInstance) {
    return _Disconnect_Base(eInstance);
  }

  /**
   * @brief Disconnect disconnects the slot from a free function.
   * @param eFunction is the free function that will be disconnected.
   */
  template <typename N>
  DisconnectionResult Disconnect(N (*eFunction)(Args...)) {
    if (m_Destroying) {
      return DisconnectionResult::NotNecessary;
    }
    auto tempSlot = ktnSlot_FreeFunction<N>(eFunction);
    if (m_CallingCurrentSlots) {
      auto guard = std::lock_guard(m_Mutex_Slots);
      for (auto &connection : m_Connections) {
        if (tempSlot == connection.Slot) {
          connection.PendingRemoval = true;
          m_ConnectionsModified = true;
          return DisconnectionResult::Success;
        }
      }
      return DisconnectionResult::NotNecessary;
    }

    auto guard_connections = std::lock_guard(m_Mutex_Connections);
    auto guard_slot = std::lock_guard(m_Mutex_Slots);
    auto connectionMatch = [&tempSlot](Connection &connection) {
      if (tempSlot == connection.Slot) {
        delete connection.Slot;
        return true;
      }
      return false;
    };

    auto iterator = std::remove_if(m_Connections.begin(), m_Connections.end(), connectionMatch);

    // This check must be done, because erasing end() blocks indefinitely
    if (iterator == m_Connections.end()) {
      return DisconnectionResult::NotNecessary;
    }
    m_Connections.erase(iterator);

    return DisconnectionResult::Success;
  }

  /// @brief Disconnects the signal from all slots.
  void DisconnectAll() {
    if (m_Destroying) {
      return;
    }
    if (m_CallingCurrentSlots) {
      auto guard_slot = std::lock_guard(m_Mutex_Slots);
      for (auto &connection : m_Connections) {
        connection.PendingRemoval = true;
      }
      m_ConnectionsModified = true;
      return;
    }

    auto guard_connections = std::lock_guard(m_Mutex_Connections);
    auto guard_slot = std::lock_guard(m_Mutex_Slots);
    for (auto &connection : m_Connections) {
      if (connection.Slot->Context() != nullptr) {
        connection.Slot->Context()->_Dissociate(this);
      }
      delete connection.Slot;
    }
    m_Connections.clear();
  }

  /**
   * @brief DisconnectByID disconnects a slot from a connection based on its ID.
   * While it can be used to remove all kinds of connections,
   * it is the only way to remove connections to lambdas, without using DisconnectAll()
   * @param eConnectionID is the ID of the connection to be disconnected.
   */
  DisconnectionResult DisconnectByID(const size_t &eConnectionID) {
    if (m_Destroying) {
      return DisconnectionResult::NotNecessary;
    }
    if (m_CallingCurrentSlots) {
      auto guard = std::lock_guard(m_Mutex_Slots);
      for (auto &connection : m_Connections) {
        if (eConnectionID == connection.ID) {
          connection.PendingRemoval = true;
          m_ConnectionsModified = true;
          return DisconnectionResult::Success;
        }
      }
      return DisconnectionResult::NotNecessary;
    }

    auto guard_connections = std::lock_guard(m_Mutex_Connections);
    auto guard_slot = std::lock_guard(m_Mutex_Slots);

    Base *instance = nullptr;
    auto idMatch = [&](Connection &connection) {
      if (connection.ID == eConnectionID) {
        if (connection.Slot->Context() != nullptr) {
          instance = connection.Slot->Context();
        }
        return true;
      }
      return false;
    };

    auto i = std::find_if(m_Connections.begin(), m_Connections.end(), idMatch);

    // This check must be done, because erasing end() blocks indefinitely
    if (i == m_Connections.end()) {
      return DisconnectionResult::NotNecessary;
    }

    if (instance != nullptr) {
      size_t instanceCount = 0;
      for (auto &connection : m_Connections) {
        if (connection.Slot->Context() == i->Slot->Context()) {
          ++instanceCount;
        }
      }
      if (instanceCount == 1) instance->_Dissociate(this);
    }

    delete i->Slot;
    m_Connections.erase(i);
    return DisconnectionResult::Success;
  }

  /**
   * @brief Disconnects the signal from all slots associated with a specific receiver.
   * @param eReceiver is the signal receiver to which all connections are to be removed.
   */
  DisconnectionResult DisconnectReceiver(ktnSignalReceiver *eReceiver) {
    return _Disconnect_Base(eReceiver);
  }

  /**
   * @brief Emits the signal.
   * @param eArgs is the argument pack to be used by the slots.
   */
  void Emit(Args... eArgs) noexcept {
    if (m_Destroying) return;
    if (m_CallingCurrentSlots) return;
    auto guardConnection = std::lock_guard(m_Mutex_Connections);

    m_CallingCurrentSlots = true;
    for (size_t i = 0; i < m_Connections.size(); ++i) {
      auto &connection = m_Connections[i];
      if (connection.PendingRemoval) {
        continue;
      }
      if (!connection.IsActive) {
        continue;
      }
      connection.Slot->Execute(eArgs...);
    }
    m_CallingCurrentSlots = false;
    if (!m_ConnectionsModified) {
      return;
    }

    auto guard_Slots = std::lock_guard(m_Mutex_Slots);

    if (m_Destroying) return;

    _RemoveMarkedConnections_NoLock();
    m_ConnectionsModified = false;
  }

private:
  /// @brief The IktnSlot class describes the interface of different slot types.
  class IktnSlot {
    IktnSlot(IktnSlot &) = delete;
    IktnSlot(IktnSlot &&) = delete;
    IktnSlot &operator=(IktnSlot &) = delete;
    IktnSlot &operator=(IktnSlot &&) = delete;

  public:
    virtual ~IktnSlot() = default;
    virtual bool operator==(IktnSlot *eSlot) const = 0;

    virtual void Execute(Args...) = 0;

    /**
     * @brief The context of a slot. This has different meaning depending on
     * each derived type, but there is one thing in common: if a context
     * exists, and is deleted, the associated slot will be removed as well.
     */
    virtual Base *Context() = 0;
    [[nodiscard]] virtual bool SetContext(Base * /*eInstance*/) {
      return false;
    }

  protected:
    IktnSlot() = default;
  };

  /**
   * @brief The ktnSlot_ClassMember class describes the type of slot
   * corresponding to an object's member function.
   */
  template <class Receiver, typename SlotReturnType, class = IsBased<ktnSignalReceiver, Receiver>>
  class ktnSlot_ClassMember final : public IktnSlot {
  public:
    ktnSlot_ClassMember(Receiver *eInstance, SlotReturnType (Receiver::*eFunction)(Args...)) {
      m_Instance = eInstance;
      m_Function = eFunction;
    }

    ~ktnSlot_ClassMember() final = default;

    bool operator==(IktnSlot *eSlot) const final {
      auto eSlotCast = dynamic_cast<ktnSlot_ClassMember *>(eSlot);
      if (eSlotCast == nullptr) {
        return false;
      } // type mismatch
      return m_Instance == eSlotCast->m_Instance && m_Function == eSlotCast->m_Function;
    }

    bool SameFunction(IktnSlot *eSlot) const {
      auto eSlotCast = dynamic_cast<ktnSlot_ClassMember *>(eSlot);
      if (eSlotCast == nullptr) {
        return false;
      } // type mismatch
      return m_Function == eSlotCast->m_Function;
    }

    /**
     * @brief Execute executes the function.
     * @param eArguments is the argument pack given when the signal emits.
     */
    void Execute(Args... eArguments) final {
      (m_Instance->*m_Function)(eArguments...);
    }

    /**
     * @brief Context
     * @return the pointer to the associated receiver.
     */
    Base *Context() final {
      return m_Instance;
    }

    [[nodiscard]] bool SetContext(Base *eInstance) override {
      m_Instance = (Receiver *)eInstance;
      return true;
    }

  private:
    Receiver *m_Instance = nullptr;
    SlotReturnType (Receiver::*m_Function)(Args...) = nullptr;
  };

  /// @brief The ktnSlot_FreeFunction class describes the slot corresponding to a free function.
  template <typename SlotReturnType>
  class ktnSlot_FreeFunction final : public IktnSlot {
  public:
    explicit ktnSlot_FreeFunction(SlotReturnType (*eFunction)(Args...)) {
      m_Function = eFunction;
    }

    ~ktnSlot_FreeFunction() final = default;

    bool operator==(IktnSlot *eSlot) const final {
      auto slotCast = dynamic_cast<ktnSlot_FreeFunction *>(eSlot);
      if (slotCast == nullptr) {
        return false;
      } // type mismatch
      return m_Function == slotCast->m_Function;
    }

    /**
     * @brief Execute executes the function.
     * @param eArguments is the argument pack given when the signal emits.
     */
    void Execute(Args... eArguments) final {
      m_Function(eArguments...);
    }

    /**
     * @brief Context
     * @return nullptr since there is no context for a free function.
     */
    ktnSignalReceiver *Context() final {
      return nullptr;
    }

    SlotReturnType (*m_Function)(Args...) = nullptr;
  };

  /// @brief The ktnSlot_Lambda class describes the slot corresponding to a lambda expression.
  template <typename Lambda>
  class ktnSlot_Lambda final : public IktnSlot {
    using ReturnType = std::invoke_result_t<Lambda, Args...>;

  public:
    ktnSlot_Lambda(ktnSignalReceiver *eContext, Lambda eLambda) : m_Function(eLambda) {
      m_Context = eContext;
    }

    ~ktnSlot_Lambda() final = default;

    bool operator==(IktnSlot *eSlot) const final {
      return false;
    }

    void Execute(Args... eArguments) final {
      m_Function(eArguments...);
    }

    /**
     * @brief Context
     * @return the pointer to the associated context object.
     */
    ktnSignalReceiver *Context() final {
      return m_Context;
    }

    const std::function<ReturnType(Args...)> m_Function;
    ktnSignalReceiver *m_Context = nullptr;
  };

  /**
   * @brief The ktnSlot_Repeater class describes the slot corresponding to the repeater
   * when 2 signals are connected for repetition.
   * @details When there are many layers between the emission of a signal and its reaction,
   * intermediate signals can be used inbetween to transmit the original signal.
   * With this class, the repeater can be managed in the same manner as other slots.
   */
  class ktnSlot_Repeater final : public IktnSlot {
  public:
    ktnSlot_Repeater(ktnSignal *eInstance) {
      m_Instance = eInstance;
    }
    bool operator==(IktnSlot *eSlot) const final {
      auto slotCast = dynamic_cast<ktnSlot_Repeater *>(eSlot);
      if (slotCast == nullptr) {
        return false;
      } // type mismatch
      return m_Instance == slotCast->m_Instance;
    }

    void Execute(Args... args) final {
      m_Instance->Emit(args...);
    }

    /**
     * @brief Context
     * @return the pointer to the associated repeater signal.
     */
    Base *Context() final {
      return m_Instance;
    }

    [[nodiscard]] bool SetContext(Base *eInstance) final {
      auto temp = dynamic_cast<ktnSignal *>(eInstance);
      if (temp != nullptr) {
        m_Instance = temp;
        return true;
      }

      return false;
    }

  private:
    ktnSignal *m_Instance;
  };

  /**
   * @brief The ktnSlot_Signal class describes the slot corresponding to the original signal
   * when 2 signals are connected for repetition.
   * @details When there are many layers between the emission of a signal and its reaction,
   * intermediate signals can be used inbetween to transmit the original signal.
   * With this class, the original signal can be managed in the same manner as other slots.
   */
  class ktnSlot_Signal final : public IktnSlot {
  public:
    ktnSlot_Signal(ktnSignal *eInstance) {
      m_Instance = eInstance;
    }
    bool operator==(IktnSlot *eSlot) const final {
      auto slotCast = dynamic_cast<ktnSlot_Signal *>(eSlot);
      if (slotCast == nullptr) {
        return false;
      } // type mismatch
      return m_Instance == slotCast->m_Instance;
    }

    void Execute(Args... /*args*/) final {}

    /**
     * @brief Context
     * @return the pointer to the associated original signal.
     */
    Base *Context() final {
      return m_Instance;
    }

    [[nodiscard]] bool SetContext(Base *eInstance) final {
      auto temp = dynamic_cast<ktnSignal *>(eInstance);
      if (temp != nullptr) {
        m_Instance = temp;
        return true;
      }

      return false;
    }

  private:
    ktnSignal *m_Instance;
  };

  /// @brief Contains the ID of the connection and the associated slot.
  struct Connection {
    Connection(size_t id, IktnSlot *slot, bool isActive) : ID(id), Slot(slot), IsActive(isActive) {}
    Connection(Connection &other) : ID(other.ID), Slot(other.Slot), IsActive(other.IsActive) {}
    Connection(Connection &&other)
        : ID(other.ID), Slot(other.Slot), IsActive(other.IsActive.load()) {}

    void operator=(Connection &other) {
      ID = other.ID;
      Slot = other.Slot;
    }

    void operator=(Connection &&other) {
      ID = other.ID;
      Slot = other.Slot;
    }

    size_t ID; //!< The ID of the connection.
    IktnSlot *Slot = nullptr; //!< The associated slot.

    /**
     * @brief IsActive indicates whether the connection is ready to be called.
     * It only evaluates to false in one case: a connection is added during emission.
     * In this situation, the connection is added to the list, but the slot is ignored.
     * After the signal finishes calling old slots that existed before
     * and removing signals disconnected during the call of Emit,
     * all remaining connections will be set to active.
     */
    std::atomic_bool IsActive = false;
    std::atomic_bool PendingRemoval = false;
  };

  //!< Returns true if a duplicate slot is found, false otherwise.
  bool _CheckDuplicate_NoLock(IktnSlot *eSlot, size_t &ID) const {
    for (auto &connection : m_Connections) {
      if (*eSlot == connection.Slot) {
        ID = connection.ID;
        return true;
      }
    }
    return false;
  }

  [[nodiscard]] std::set<size_t> _ConnectionIDs_NoLock() const {
    std::set<size_t> connectionIDs;
    for (auto &connection : m_Connections) {
      connectionIDs.insert(connection.ID);
    }
    return connectionIDs;
  }

  template <bool RequiresRefManagement>
  const ConnectionResultWithId _Connect_DuringEmission_NoLock(IktnSlot *eSlot) {
    size_t id = 0;
    if (_CheckDuplicate_NoLock(eSlot, id)) {
      delete eSlot;
      return {id, ConnectionResult::Duplicate};
    }
    id = _NextUnusedConnectionID();
    if constexpr (RequiresRefManagement) {
      eSlot->Context()->_AddSignal(this);
    }

    m_Connections.emplace_back(Connection{id, eSlot, false});
    m_ConnectionsModified = true;
    return {id, ConnectionResult::Success};
  }

  template <bool RequiresRefManagement>
  const ConnectionResultWithId _Connect_WhileIdle_NoLock(IktnSlot *eSlot) {
    size_t id = 0;
    if (m_Connections.empty()) {
      if constexpr (RequiresRefManagement) {
        eSlot->Context()->_AddSignal(this);
      }
      m_Connections.emplace_back(Connection{1, eSlot, true});
      return {1, ConnectionResult::Success};
    }

    if (_CheckDuplicate_NoLock(eSlot, id)) {
      delete eSlot;
      return {id, ConnectionResult::Duplicate};
    }

    id = _NextUnusedConnectionID();
    if constexpr (RequiresRefManagement) {
      eSlot->Context()->_AddSignal(this);
    }
    m_Connections.emplace_back(Connection{id, eSlot, true});
    return {id, ConnectionResult::Success};
  }

  template <class C, typename N, class = IsBased<ktnSignalReceiver, C>>
  ConnectionResultWithId _Connect_MemberFunction(C *eInstance, N (C::*eFunction)(Args...)) {
    if (m_Destroying) {
      return {0, ConnectionResult::Failure};
    }
    if (eInstance == nullptr) {
      return {0, ConnectionResult::Failure};
    } // invalid pointer
    auto tempSlot = new ktnSlot_ClassMember<C, N>(eInstance, eFunction);

    if (m_CallingCurrentSlots) {
      auto guard_Slots = std::lock_guard(m_Mutex_Slots);
      return _Connect_DuringEmission_NoLock<true>(tempSlot);
    }

    auto guard_Connections = std::lock_guard(m_Mutex_Connections);
    auto guard_Slots = std::lock_guard(m_Mutex_Slots);
    return _Connect_WhileIdle_NoLock<true>(tempSlot);
  }

  template <bool AsRepeater>
  ConnectionResultWithId _Connect_Signal(ktnSignal *eInstance) {
    if (m_Destroying) {
      return {0, ConnectionResult::Failure};
    };
    if (eInstance == nullptr) {
      return {0, ConnectionResult::Failure};
    } // invalid pointer
    if (eInstance == this) {
      return {0, ConnectionResult::Failure};
    }
    IktnSlot *tempSlot;
    if constexpr (AsRepeater)
      tempSlot = new ktnSlot_Repeater(eInstance);
    else
      tempSlot = new ktnSlot_Signal(eInstance);

    if (m_CallingCurrentSlots) {
      auto guard_Slots = std::lock_guard(m_Mutex_Slots);
      return _Connect_DuringEmission_NoLock<AsRepeater>(tempSlot);
    }

    auto guard_Connections = std::lock_guard(m_Mutex_Connections);
    auto guard_Slots = std::lock_guard(m_Mutex_Slots);
    return _Connect_WhileIdle_NoLock<AsRepeater>(tempSlot);
  }

  DisconnectionResult _Disconnect_Base(::ktn::SignalSlot::Base *eInstance) {
    if (m_Destroying) {
      return DisconnectionResult::NotNecessary;
    }
    if (eInstance == nullptr) {
      return DisconnectionResult::NotNecessary;
    }
    if (m_Connections.empty()) {
      return DisconnectionResult::NotNecessary;
    }

    if (m_CallingCurrentSlots) {
      auto guard_Slots = std::lock_guard(m_Mutex_Slots);
      bool removableConnectionsFound = false;
      for (auto &connection : m_Connections) {
        if (eInstance == connection.Slot->Context()) {
          connection.PendingRemoval = true;
          m_ConnectionsModified = true;
          removableConnectionsFound = true;
        }
      }
      if (removableConnectionsFound) {
        eInstance->_Dissociate(this);
        return DisconnectionResult::Success;
      }
      return DisconnectionResult::NotNecessary;
    }

    auto guard_Connections = std::lock_guard(m_Mutex_Connections);
    auto guard_Slots = std::lock_guard(m_Mutex_Slots);

    bool removableConnectionsFound = false;
    auto receiverMatch = [&eInstance, &removableConnectionsFound](Connection &connection) {
      if (connection.Slot->Context() == eInstance) {
        delete connection.Slot;
        removableConnectionsFound = true;
        return true;
      }
      return false;
    };

    auto iterator = std::remove_if(m_Connections.begin(), m_Connections.end(), receiverMatch);
    eInstance->_Dissociate(this);

    if (removableConnectionsFound) {
      m_Connections.erase(iterator, m_Connections.end());
      return DisconnectionResult::Success;
    }

    return DisconnectionResult::NotNecessary;
  }

  template <class C, typename N, class = IsBased<ktnSignalReceiver, C>>
  DisconnectionResult _Disconnect_MemberFunction(C *eInstance, N (C::*eFunction)(Args...)) {
    if (m_Destroying) {
      return DisconnectionResult::NotNecessary;
    }
    auto tempSlot = ktnSlot_ClassMember<C, N>(eInstance, eFunction);

    if (m_CallingCurrentSlots) {
      auto guard_Slots = std::lock_guard(m_Mutex_Slots);
      for (auto &connection : m_Connections) {
        if (tempSlot == connection.Slot) {
          connection.PendingRemoval = true;
          m_ConnectionsModified = true;
          return DisconnectionResult::Success;
        }
      }
      return DisconnectionResult::NotNecessary;
    }

    auto guard_connections = std::lock_guard(m_Mutex_Connections);
    auto guard_Slots = std::lock_guard(m_Mutex_Slots);
    size_t instanceCount = 0;
    auto connectionMatch = [&](Connection &connection) {
      if (tempSlot.Context() != connection.Slot->Context()) return false;
      ++instanceCount;
      if (tempSlot.SameFunction(connection.Slot)) {
        delete connection.Slot;
        return true;
      }
      return false;
    };

    auto iterator = std::remove_if(m_Connections.begin(), m_Connections.end(), connectionMatch);

    switch (instanceCount) {
    case 0:
      return DisconnectionResult::NotNecessary;
    case 1:
      tempSlot.Context()->_Dissociate(this);
      [[fallthrough]];
    default:
      m_Connections.erase(iterator, m_Connections.end());
      return DisconnectionResult::Success;
    }
  }

  /**
   * @brief _NextUnusedConnectionID finds a suitable ID for a new connection.
   * @return  the lowest number that can be assigned to a new connection.
   */
  [[nodiscard]] size_t _NextUnusedConnectionID() const {
    auto connections = _ConnectionIDs_NoLock();
    if (connections.empty()) {
      return 1;
    }
    auto maxConnectionID = *std::max_element(connections.begin(), connections.end());
    for (size_t i = 0; i < maxConnectionID; ++i) {
      if (connections.end() == std::find(connections.begin(), connections.end(), i + 1)) {
        return i + 1;
      }
    }
    return maxConnectionID + 1;
  }

  void _RemoveMarkedConnections_NoLock() {
    std::set<IktnSlot *> removables; // removable connections
    auto isRemovable = [&removables](Connection &connection) -> bool {
      if (connection.PendingRemoval) {
        removables.insert(connection.Slot);
        return true;
      }
      return false;
    };

    auto iterator = std::remove_if(m_Connections.begin(), m_Connections.end(), isRemovable);
    if (iterator != m_Connections.end()) {
      m_Connections.erase(iterator, m_Connections.end());
    }

    std::for_each(removables.begin(), removables.end(), [this](IktnSlot *removed) {
      auto contextRemains = [&removed](auto &connection) {
        if (connection.Slot->Context() == removed->Context()) {
          return true;
        }
        return false;
      };

      auto found = std::find_if(m_Connections.begin(), m_Connections.end(), contextRemains);
      if (found == m_Connections.end()) {
        if (removed->Context() != nullptr) {
          removed->Context()->_Dissociate(this);
        }
      }

      delete removed;
    });

    for (auto &connection : m_Connections) {
      if (!connection.IsActive) connection.IsActive = true;
    }
  }

  /**
   * @brief _AddSignal
   * @details When a signal is used as a repeater,
   * the original signal has to be added on the repeater's side to ensure proper cleanup.
   * @param eSignal
   */
  void _AddSignal(Base *eSignal) final {
    auto signalCast = dynamic_cast<ktnSignal *>(eSignal);
    _Connect_Signal<false>(signalCast);
  }

  void _HandleAssociateMoved(Base *from, Base *to) final {
    auto guard_Connections = std::lock_guard(m_Mutex_Connections);
    auto guard_Slots = std::lock_guard(m_Mutex_Slots);
    auto assignNewReceiver = [&from, &to, this](Connection &connection) {
      if (connection.Slot->Context() == from) {
        auto newInstanceIsSet = connection.Slot->SetContext(to);
        if (false == newInstanceIsSet) {
          connection.IsActive = false;
          connection.PendingRemoval = true;
          m_ConnectionsModified = true;
        }
      }
    };
    std::for_each(m_Connections.begin(), m_Connections.end(), assignNewReceiver);

    if (m_ConnectionsModified) {
      return;
    }
    _RemoveMarkedConnections_NoLock();
    m_ConnectionsModified = false;
  }

  void _Dissociate(Base *target) final {
    auto guard_Connections = std::lock_guard(m_Mutex_Connections);
    auto guard_Slots = std::lock_guard(m_Mutex_Slots);

    auto receiverMatch = [&target](Connection &connection) {
      if (connection.Slot->Context() == target) {
        delete connection.Slot;
        return true;
      }
      return false;
    };

    auto iterator = std::remove_if(m_Connections.begin(), m_Connections.end(), receiverMatch);

    if (iterator != m_Connections.end()) {
      m_Connections.erase(iterator, m_Connections.end());
    }
  }

  mutable std::mutex m_Mutex_Connections;
  mutable std::mutex m_Mutex_Slots;

  /// @brief The list of current connections.
  std::vector<Connection> m_Connections;

  /// @brief Indicates if the signal is calling its slots during emission.
  std::atomic_bool m_CallingCurrentSlots = false;

  /**
   * @brief Is true when a connection is added or marked as PendingRemoval
   * during a signal emission.
   */
  std::atomic_bool m_ConnectionsModified = false;

  /**
   * @brief m_Destroying only turns true in the destructor.
   * During destruction, any attempt to connect or disconnect will fail.
   */
  std::atomic_bool m_Destroying = false;
};
} // namespace ktn::SignalSlot
#ifdef KTNSIGNALSLOT_SIMPLE_TYPEDEFS
template <typename... Args>
using ktnSignal = ktn::SignalSlot::ktnSignal<Args...>;
using ktnSignalReceiver = ktn::SignalSlot::ktnSignalReceiver;
#endif // KTNSIGNALSLOT_SIMPLE_TYPEDEFS
#endif // KTNSIGNALSLOT_KTNSIGNALSLOT_HPP
