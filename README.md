# KtnSignalSlot
## Introduction
This is a thread-safe, C++17 header-only library implementing the Signals and
Slots mechanism to help connect otherwise unrelated objects.

## Supported platforms
Even though any C++17 compiler should be able to compile this library,
only a handful were tested on Linux and Windows.

### Linux (5.0 or later)
- Clang 5 or later
- GCC 7 or later

### Windows (10, latest version)
- Visual Studio 2017 or later
- MinGW 8.1.0 or later

# Features
## Connections
- Connection to member functions and function pointers (free functions,
no-capture lambda expressions)
- Connection results and IDs reporting
- Duplication check
- Automatic removal of connections to deleted receivers

## Disconnections
- Flexible disconnection methods:
  - Disconnect using connection IDs
  - Disconnect by specifying receivers and slots
  - Disconnect from all slots associated with specific receivers
  - Disconnect all connections
- Disconnection results reporting

## Thread Safety
- Protection against race condition during connection/disconnection
- Protection against race condition during signal emission

# Behavior
- All calls to Connect(...) returns connection results, as well as their IDs.
- Connecting to the same slot multiple times does not create more than one
connection.
- During signal emissions, slots are called in the order of connection. Slots
that are connected first, get called first.
- Slots connected during signal emissions are not called until the next emission.
- Slots disconnected during signal emissions are not called.

# Usage
- Copy [ktnSignalSlot.hpp](include/ktnSignalSlot/ktnSignalSlot.hpp)
to your project's include directories.
- Include `ktnSignalSlot.hpp`.
To use `ktnSignals` and `ktnSignalReceiver` without the `ktn::SignalSlot`
namespace, define `KTNSIGNALSLOT_SIMPLE_TYPEDEFS` before inclusion.
```cpp
#include <ktnSignalSlot/ktnSignalSlot.hpp>
```

## Using class members as slots
- Derive signal receivers from `ktn::SignalSlot::ktnSignalReceiver`.
```cpp
class Receiver : public ktn::SignalSlot::ktnSignalReceiver {
public:
    void Slot(int param_from_signal) {
        // do something with the given parameters
    }
}
```
- Create signals.
- Connect signals and slots.
- Emit signals. The slots are then called in the order in which they are connected.
- Disconnect signals when no longer needed.
```cpp
using ktn::SignalSlot::ktnSignal;

int main() {
    ktnSignal<int> signal;
    auto receiver = std::make_unique<Receiver>();
    signal.Connect(receiver.get(), &Receiver::Slot);
    signal.Emit(0);
    return 0;
}
```
## Using free functions as slots
- Define free functions with the same signature
as the signals to which you want them to connect.
```cpp
void func(int param_from_signal) {
    // do things with param_from_signal
}
```
- Connect signals and slots.
```cpp
signal.Connect(&func);
```
- Emit signals or disconnect whenever needed.
```cpp
signal.Emit(9999);
signal.Disconnect(&func);
```

## Using lambda expressions as slots
- A connection with lambda expression requires a context object.
- The context object's class must be derived from ktnSignalReceiver.
- For lambda connections, disconnections have to be done via the returned ID,
or deletion of the context object, or mass disconnect functions.
- Alternatively, the connection is also removed when the context object is deleted.
```cpp
class Receiver : public ktnSignalReceiver {
public:
    int callCount = 0;
} receiver;

auto connectionResult_1 = signal.Connect(&receiver, [&receiver] () {
    ++receiver.callCount;
});
auto connectionID_1 = connectionResult_1.ID;
```
- Disconnect using the connection IDs.
```cpp
signal.DisconnectByID(connectionID_1);
```

## Examples
- Check [examples](examples/) for more usages.
```bash
mkdir build
cd build
cmake -DKTNSIGNALSLOT_BUILD_EXAMPLES=ON ..
make -j$(nproc)
```

## Other usages
- If your use case is not yet covered, feel free to suggest a change or submit a
merge request.

# License
Full text at [LICENSE](LICENSE).
```
Copyright 2019-2024 Tien Dat Nguyen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
